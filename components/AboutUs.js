import React ,{Component} from 'react';
import {Text,StyleSheet,View, ColorPropType, Image,Animated, TouchableOpacity, AsyncStorage,ScrollView} from 'react-native';
import axios from 'axios';
import { Button } from './common';
import { connect } from 'react-redux';
var customData = require('./string/AboutUs.json');

var data = customData.English

class AboutUs extends Component{
    
   
    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{
            data=customData. English
        
            }
    }

    static navigationOptions = ({ navigation }) => {
        
                const { params = {} } = navigation.state;
    
        
                if (params.reverse) {
                    return ({ title: data.x1 });
                }
                else {
                    return ({  title: data.x1 });
                }
            }

     start(){
        this.props.navigation.navigate('Profile');
     }
    
    render(){
         
        return(
            <ScrollView style={styles.cont}>
                
                <View style={{ justifyContent : 'center',alignItems : 'center',}}>
                <Image source={require('./photo/s.png')} style={styles.logo} />
                </View>
                <Text style={styles.contt}>{data.x1}</Text>
                <Text style={styles.contt}>{data.x2}</Text>
                
            </ScrollView>
            )}
}

const styles=StyleSheet.create({

    cont:{
       
        backgroundColor:'#fff', 
        padding:10,
       
    },
    logo: {
        width: 70,
        height: 90,
        margin: 20,
       
    },

})

const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        reverse: state.lang.reverse
        
    }
}

export default connect(mapStateToProps,{ })(AboutUs);
