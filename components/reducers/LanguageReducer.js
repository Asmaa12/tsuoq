import {
    ENGLISH,
    ARABIC
}from '../actions/types';
import {AsyncStorage} from 'react-native'


const initial_state = { language : '', reverse:false};




export default (state= initial_state , action) =>{
    
    switch(action.type){
        case ENGLISH:
            
            return{...state ,language:'English', reverse:action.reverse};
        
        case ARABIC:
            
            return {...state , language:'Arabic', reverse:action.reverse }


        default:{
          
            return state;
        
           
        }
    }

}