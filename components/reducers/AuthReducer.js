
import {
    LOGIN_ATTEMPT,
    LOGIN_FAILED,
    LOGIN_SUCCESS
}from '../actions/types';

const initial_state = { user: null ,name: null, loading: false ,error:'',token:''};

export default (state= initial_state , action) =>{
    switch(action.type){
        case LOGIN_ATTEMPT:
            return{...state,loading : true };
        
        case LOGIN_FAILED:
            return {...initial_state , loading: false , error:action.error}

        case LOGIN_SUCCESS:
            return {...initial_state , loading: false , user:action.user,name:action.name ,token:action.token}

        default:
        return state;
    }

}