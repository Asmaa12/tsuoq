import {
    SIGNUP_ATTEMPT,
    SIGNUP_FAILED,
    SIGNUP_SUCCESS
}from '../actions/types';

const initial_state = {  loading: false ,error:'',token:''};

export default (state= initial_state , action) =>{
    switch(action.type){
        case SIGNUP_ATTEMPT:
            return{...state,loading : true };
        
        case SIGNUP_FAILED:
            return {...initial_state , loading: false , error:action.error}

        case SIGNUP_SUCCESS:
            return {...initial_state , loading: false ,token:action.token  }

        default:
        return state;
    }

}