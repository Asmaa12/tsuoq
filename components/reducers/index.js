import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import SignReducer from './SignReducer';
import emailVerifyReducer from './emailVerifyReducer';
import changeLanguage from './LanguageReducer'


export default combineReducers (
    {
        auth : AuthReducer,
        sign : SignReducer,
        email : emailVerifyReducer,
        lang : changeLanguage,
      
    }
);