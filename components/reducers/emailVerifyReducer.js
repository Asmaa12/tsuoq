
import {
    EMAILVERIFY_ATTEMPT,
    EMAILVERIFY_FAILED,
    EMAILVERIFY_SUCCESS
}from '../actions/types';

const initial_state = { valid: null , loading: false ,error:''};

export default (state= initial_state , action) =>{
    switch(action.type){
        case EMAILVERIFY_ATTEMPT:
            return{...state,loading : true };
        
        case EMAILVERIFY_FAILED:
            return {...initial_state , loading: false , error:action.error}

        case EMAILVERIFY_SUCCESS:
            return {...initial_state , loading: false , valid:action.valid,error:'' }

        default:
        return state;
    }

}