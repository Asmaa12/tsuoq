import {
    SIGNUP_ATTEMPT,
    SIGNUP_FAILED,
    SIGNUP_SUCCESS
}from './types';

import {AsyncStorage} from 'react-native';
import axios from 'axios';
//import RNLanguages from 'react-native-languages';
//var base64 = require('base-64');

const signup =({user,pass,name,gender,birth,email,phone,work,avatar}) => {
    
    
    return(dispatch)=>{
        dispatch({type: SIGNUP_ATTEMPT});
        
        axios.post('https://streetpal.org/api/user',{user,pass,name,gender,
        birth:{d: Number(birth.split("-")[2]),m:Number(birth.split("-")[1]),y:Number(birth.split("-")[0])},
        email,phone,work})
            .then(resp=>{
                //console.log(resp),
                //console.log(avatar)
                handleResponse(dispatch,resp.data,avatar,user,name)}
            )
            .catch(err=>{console.log(err.response.data)
                        signupFailed(dispatch ,err.response.data.errors)}
            )
        
        
        };
}

const handleResponse = (dispatch , data,avatar,user,name)=>{
    console.log(data),
    AsyncStorage.setItem('app_token',data.token)
            .then(
                axios.post('https://streetpal.org/api/me/email-verify/request',{},{ 'headers': { 'Authorization': 'Bearer ' + data.token } })
                .then(resp=>{
                    //console.log('sent'),
                    AsyncStorage.setItem('username',user)
                    AsyncStorage.setItem('name',name) 
                    signSuccess(dispatch,data.token)
                    if(avatar!=null){
                        axios.post('https://streetpal.org/api/me/photo',{base64:avatar},{ 'headers': { 'Authorization': 'Bearer ' + data.token ,'Content-Type':'application/json'} })
                        .then(resp=>{
                            //console.log('uploaded')
                        
                        })
                        .catch(err=>{}
                            //console.log(err.response.data)
                        )   }
                    })  
                 .catch(err=> {}
                    //console.log(err.response.data)
                 
             )
            )
        
    };
    
    const signSuccess =(dispatch,token)=>{
        dispatch({type:SIGNUP_SUCCESS,token})
        
    
    };
    
    
    const signupFailed = (dispatch , error)=>{

        if(error.email){
            var x='email already exist'
           
            dispatch({type:SIGNUP_FAILED ,error :x})}

        else if(error.phone){
            var x='phone already exist'
            
            dispatch({type:SIGNUP_FAILED ,error :x})}

        else if(error.user){
            var x='username already exist'
            
            dispatch({type:SIGNUP_FAILED ,error :x})}

        else{
            var x='error'
           
            dispatch({type:SIGNUP_FAILED ,error :x})}
    };
    
    
    

   

export {signup};