export const LOGIN_ATTEMPT ='login_attempt';
export const LOGIN_SUCCESS ='login_success';
export const LOGIN_FAILED ='login_failed';

export const SIGNUP_ATTEMPT ='sign_attempt';
export const SIGNUP_SUCCESS ='sign_success';
export const SIGNUP_FAILED ='sign_failed';

export const EMAILVERIFY_ATTEMPT ='email_attempt';
export const EMAILVERIFY_SUCCESS ='email_success';
export const EMAILVERIFY_FAILED ='email_failed';

export const ENGLISH ='english';
export const ARABIC ='arabic';



