import {
    LOGIN_ATTEMPT,
    LOGIN_FAILED,
    LOGIN_SUCCESS
}from './types';

import {AsyncStorage} from 'react-native';
import axios from 'axios';
//import RNLanguages from 'react-native-languages';

const loginU =({username,password}) => {
    
    return(dispatch)=>{
        dispatch({type: LOGIN_ATTEMPT});
        
        axios.post('https://streetpal.org/api/authenticate',{user:username , pass:password})
            .then(resp=>handleResponse(dispatch,resp.data))
            .catch(err=> {
                var x ='accout not found or network error'
                
                loginFailed(dispatch ,x)
            })
        
        };
}

const handleResponse = (dispatch , data)=>{

    if(!data.token){
        loginFailed(dispatch ,'account not found')
    }

    else{
        const AuthStr = 'Bearer ' + data.token

        
       // .then(
            axios.get('https://streetpal.org/api/me', { 'headers': { 'Authorization': AuthStr } })
            .then(resp=>{
                loginSuccess(dispatch,resp.data.user,resp.data.name,resp.data.token),
                AsyncStorage.setItem('app_token',data.token)
                AsyncStorage.setItem('username',resp.data.user) ,
                AsyncStorage.setItem('name',resp.data.name) })  
            .catch(err=>{}
                //console.log(err.response.data)
            )  
        //)
    }
};

const loginSuccess =(dispatch ,user,name,token)=>{
    dispatch({type:LOGIN_SUCCESS,user,name,token})
    

};


const loginFailed = (dispatch , errorMassage)=>{
    dispatch({type:LOGIN_FAILED ,error :errorMassage})

};

export {loginU};



