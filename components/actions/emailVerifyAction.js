import {
    EMAILVERIFY_ATTEMPT,
    EMAILVERIFY_FAILED,
    EMAILVERIFY_SUCCESS
}from './types';
//import RNLanguages from 'react-native-languages';
import {AsyncStorage} from 'react-native';
import axios from 'axios';

const email_verify =({code}) => {
    
    return(dispatch)=>{
        dispatch({type: EMAILVERIFY_ATTEMPT});
        AsyncStorage.getItem('app_token')
        .then(token=>
            axios.post('https://streetpal.org/api/me/email-verify/respond',{code},{ 'headers': { 'Authorization': 'Bearer ' + token }})
                .then(resp=>{
                    //console.log(resp.data)
                    Success(dispatch)})
                .catch(err=> {
                    var x='incorrect code'
                    

                    Failed(dispatch ,x)})
            )

        
        }
}

    
    const Success =(dispatch )=>{
        dispatch({type:EMAILVERIFY_SUCCESS ,valid:'yes'})
        
    
    };
    
    
    const Failed = (dispatch , errorMassage)=>{
    dispatch({type:EMAILVERIFY_FAILED ,error :errorMassage})
    
    };
    


export {email_verify};