import {
    ENGLISH,
    ARABIC
}from './types';
//import RNLanguages from 'react-native-languages';
import {AsyncStorage,NativeModules,Platform} from 'react-native';


const Change_lang =({lang}) => {
      if (Platform.OS === "android") {
            locale = NativeModules.I18nManager.localeIdentifier || "";
          } else if (Platform.OS === "ios") {
            locale = NativeModules.SettingsManager.settings.AppleLocale || "";
          }
          
    if(lang=='Arabic'){
        AsyncStorage.setItem('language','Arabic') 
        
        if(locale!='ar_EG'){
            return(dispatch)=>{
                dispatch({type: ARABIC ,reverse:true});}
        }
        else{
            return(dispatch)=>{
                dispatch({type: ARABIC ,reverse:false});}
        }
    }
    else{
        AsyncStorage.setItem('language','English')

        if(locale=='ar_EG'){
            return(dispatch)=>{
                dispatch({type: ENGLISH ,reverse:true});}
        }
        else{
            return(dispatch)=>{
                dispatch({type: ENGLISH ,reverse:false});}
        }
        
        
    }   
}

    


export {Change_lang};