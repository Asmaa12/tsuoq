// import libs
import React, { Component } from 'react';
import { Text, StyleSheet, View, ColorPropType, Image, BackHandler, TouchableOpacity, ScrollView, Platform } from 'react-native';
import { connect } from 'react-redux';
import { Button, Card, CardItem, Input, Spinner } from './common';
import { loginU } from './actions';

var customData = require('./string/LoginForm.json');
var data = customData.English


class loginForm extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',

        };
    }

    componentWillMount() {
        if (this.props.lang1 == 'Arabic') {
            data = customData.Arabic

        }

        else {
            data = customData.English
        }

    }

    handleBack() {
        BackHandler.exitApp();
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.user) {
            this.props.navigation.navigate('Profile');
        }
    }

    loginpress() {
        const username = this.state.username;
        const password = this.state.password;
        this.props.loginU({ username, password });

    }

    signup() {
        this.props.navigation.navigate('SignupForm')
    }
    forget() {
        this.props.navigation.navigate('ForgetPass')
    }

    _renderButton() {
        if (this.props.loading) {

            return <Spinner />
        }
        else {


            return (
                <Button height={52} color={'#FDAC60'} onpress={this.loginpress.bind(this)}>{data.x1}</Button>
            );
        }
    }


    render() {





        return (
            <ScrollView style={styles.contain}>

                <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                    <Image source={require('./photo/s.png')} style={styles.logo} />
                    <Text style={styles.street}>Street Pal</Text>

                    <Card>
                        <CardItem>
                            <Image source={require('./photo/username.png')} style={styles.image} />
                            <Input
                                placeholder={data.x2}
                                securetext={false}
                                autoFocus={true}
                                editable={!(this.props.loading)}
                                multiline={false}
                                onChangeText={(username) => this.setState({ username })} />
                        </CardItem>

                        <CardItem>
                            <Image source={require('./photo/pass.png')} style={styles.image} />
                            <Input
                                onChangeText={(password) => this.setState({ password })}
                                placeholder={data.x3}
                                editable={!(this.props.loading)}
                                multiline={false}
                                securetext={true} />
                        </CardItem>
                    </Card>

                    <Text style={styles.error}>{this.props.error}</Text>

                    <View style={styles.bu}>
                        {this._renderButton()}
                    </View>

                    <TouchableOpacity onPress={this.signup.bind(this)} >
                        <Text style={styles.text2}>{data.x4}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.forget.bind(this)} >
                        <Text style={styles.text2}>{data.x5}</Text>
                    </TouchableOpacity>


                </View>


            </ScrollView>
        );


    }
}
const styles = StyleSheet.create({

    contain: {
        flex: 1,
        // justifyContent : 'center',
        // alignItems : 'center',
        backgroundColor: '#fff'


    },

    error: {
        color: 'red',

    },

    image: {
        marginTop: 10,
        marginLeft: 10,
        width: 28,
        height: 28,
        opacity: .4
    },

    logo: {
        width: 90,
        height: 110,
        marginTop: 40,
    },

    street: {

        fontSize: 25, fontWeight: '100', color: '#555',
        fontFamily: Platform.OS === 'ios' ? 'courier' : 'monospace',

    },
    text1: {
        fontSize: 16,
        margin: 20,
    },
    text2: {
        color: '#000', margin: 10,
        fontSize: 15, fontWeight: 'bold',
    },
    bu: {
        marginTop: 20,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flex: 1,
    },
    st: {
        marginLeft: 11, marginRight: 11
    }
});



const mapStateToProps = state => {
    return {
        error: state.auth.error,
        loading: state.auth.loading,
        user: state.auth.user,
        lang1: state.lang.language,
    }
}
//export componant for other apps
export default connect(mapStateToProps, { loginU })(loginForm);
