import React, { Component } from 'react';
import { Text, StyleSheet, View, ColorPropType, Image, Animated, TouchableOpacity, AsyncStorage, ScrollView,StatusBar } from 'react-native';
import axios from 'axios';
import { connect } from 'react-redux';
import Icon from 'react-native-fa-icons';

var customData = require('./string/Home.json');

var data = customData.English


class Home extends Component {

    constructor() {
        super();
        this.state = {
            name: '',
            username: '',

        };
    }

    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{
            data=customData. English
        
            }
    }
    
    componentDidMount() {

        AsyncStorage.getItem('name')
            .then(name => this.setState({ name: name }), )
        AsyncStorage.getItem('username')
            .then(username => {
                this.setState({ username: username })
            })

        AsyncStorage.getItem('app_token')
            .then(token =>
                axios.get('https://streetpal.org/api/me', { 'headers': { 'Authorization': 'Bearer ' + token } })
                    .then(resp => {
                        console.log(resp.data)
                        this.setState({ name: resp.data.name })
                        //if((resp.data.email-verified)){ this.props.navigation.navigate('EmailVerify')   }
                    })
                    .catch(err => { console.log('error') })
            )


    }

    componentWillUnmount() {

        AsyncStorage.getItem('name')
            .then(name => this.setState({ name: name }), )
        AsyncStorage.getItem('username')
            .then(username => {
                this.setState({ username: username })
            })

        AsyncStorage.getItem('app_token')
            .then(token =>
                axios.get('https://streetpal.org/api/me', { 'headers': { 'Authorization': 'Bearer ' + token } })
                    .then(resp => {
                        this.setState({ name: resp.data.name })
                    })
                    .catch(err => { console.log('error') })
            )


    }



    render() {
        

        return (
            <ScrollView >
                <View style={styles.cont}>
                <StatusBar
            backgroundColor={'#D59B57'}
          />
                    <View style={styles.user}>
                        <Image source={require('./photo/user.png')} style={[styles.image1, { zIndex: -1 }]} />
                        <Image source={{ uri: 'https://streetpal.org/api/user/' + (this.props.user || this.state.username) + '/photo' }} style={[styles.image1, { zIndex: 2 }]} />
                        <Text style={styles.tx}>{this.props.name || this.state.name}</Text>
                    </View>

                    <Text style={[styles.contt, { backgroundColor: '#fff' }]}>{data.x1}</Text>

                    <Text style={styles.contt}>{data.x2}</Text>

                    <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('Chat');
                    }}>
                        <Image source={require('./photo/connect.png')} style={styles.image} />
                        <Text>{data.x3}</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    contt: {

        padding: 10,
        margin: 35,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 15,
        backgroundColor: '#f5f5f5',
    },
    image1: {
        height: 85,
        width: 85,
        margin: 15,
        borderRadius: 43,
        position: 'absolute',
        top: 4

    },
    tx: {
        marginTop: 120,
        color: '#000'
    },
    user: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    image: {
        height: 70,
        width: 70,
        // marginTop: 30

    },
    cont: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        width: 27, height: 23
    }
})

const mapStateToProps =state =>{
    return {
        user : state.auth.user,
        name :state.auth.name,
        lang1: state.lang.language,
    }
}



//export componant for other apps
export default connect(mapStateToProps,{ })(Home);
