import React ,{Component} from 'react';
import {Image, View ,Text, StyleSheet,AsyncStorage,ActivityIndicator} from 'react-native';
import { connect } from 'react-redux';
import { Change_lang} from './actions';
var data 

class Splash extends Component {

    
   
    componentDidMount(){
        
        AsyncStorage.getItem('language')
        .then(lang=>{
            if(lang=='Arabic'){
                this.props.Change_lang({ lang:'Arabic'})
        }
            else{
                this.props.Change_lang({ lang:'English'})
            }
    })
        AsyncStorage.getItem('app_token')
        .then(token=>{
            if(token){
            this.props.navigation.navigate('Profile')}
           else{
               this.props.navigation.navigate('loginForm')
           }
        }

        )

      

    }
    render(){
        return (
            <View style={styles.container}>

                <Image source={require('./photo/s.png')} style={styles.image}/>
        
            </View>
        );
    }
}
const styles=StyleSheet.create({
    container :{
        flex :1,
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : '#FDAC60'
    },
    image :{
        height:90,
        width :90,
        borderRadius : 33,
    }
})


const mapStateToProps = state => {
    return {
        lang1: state.lang.language
        
    }
}

export default connect(mapStateToProps, { Change_lang})(Splash)
