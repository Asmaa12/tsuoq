import React ,{Component} from 'react';
import {Text,StyleSheet,View,ScrollView, AsyncStorage, BackHandler,TouchableOpacity } from 'react-native';
import { Title,Option1 } from './common/index';
import { connect } from 'react-redux';
var customData = require('./string/Setting.json');

var data = customData.English

class Setting extends Component{

    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{
            data=customData. English
        
            }
    }
    static navigationOptions = ()=>{
     
        return ({title:data.x12 });
     }
    
      _logout(){
        AsyncStorage.removeItem('app_token');
        AsyncStorage.removeItem('username') ;
        AsyncStorage.removeItem('name');
        this.props.navigation.navigate('loginForm');
      }

      addcontact(){
        this.props.navigation.navigate('addContact');
      }
      ChangePass(){
        this.props.navigation.navigate('ChangePass');
      }
      edit(){
        this.props.navigation.navigate('EditProfile');
      }
      language(){
        this.props.navigation.navigate('Language');
      }
      AboutUs(){
        this.props.navigation.navigate('AboutUs');
      }
      Policy(){
        this.props.navigation.navigate('Policy');
      }
     

    render(){
      
      
        return(
            <ScrollView style={styles.cont}>

                
                    <Title>{data.x1}</Title>
                    <Option1 reverse={this.props.reverse} vector={data.x13} onpress={this.edit.bind(this)}>{data.x2}</Option1>
                    <Option1 reverse={this.props.reverse}  vector={data.x13} onpress={this.ChangePass.bind(this)}>{data.x3}</Option1>
                        
                    <Title>{data.x4}</Title>
                    <Option1 reverse={this.props.reverse}  vector={data.x13} onpress={this.addcontact.bind(this)}>{data.x5} </Option1>
                    <Option1 reverse={this.props.reverse} vector={data.x13} onpress={this.language.bind(this)}>{data.x6}</Option1>    
                
 
                    <Title>{data.x7}</Title>
                    <Option1 reverse={this.props.reverse} vector={data.x13} onpress={this.AboutUs.bind(this)} >{data.x8} </Option1>
                    <Option1 reverse={this.props.reverse} vector={data.x13} onpress={this.Policy.bind(this)} >{data.x9}</Option1>    
                    <Option1 reverse={this.props.reverse} vector={data.x13}>{data.x10}</Option1>

                
                    <TouchableOpacity   onPress ={this._logout.bind(this)} >
                        <Text style={styles.logout}>{data.x11}</Text> 
                    </TouchableOpacity > 
                
                
            
            </ScrollView>
            )}
}

const styles=StyleSheet.create({
   
    cont:{
        backgroundColor : '#eee',
        
    },
    
    
    itemd:{
        backgroundColor : '#fff',
        color : '#bbb',
        fontSize : 14 ,
        fontWeight :'bold',
        padding: 10,
        paddingLeft:17,
        
    },
    
    logout :{
        backgroundColor : '#fff',
        color :'blue',
        padding: 10,
        paddingLeft:17,
        marginTop:20,
        
    }
})
const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        reverse:state.lang.reverse
    }
}

export default connect(mapStateToProps,{ })(Setting);
