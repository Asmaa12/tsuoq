import React ,{Component} from 'react';
import {Text,StyleSheet,View, Dimensions, Image,Animated, TouchableOpacity, AsyncStorage, TextInput,ScrollView} from 'react-native';
import axios from 'axios';
import Icon from 'react-native-fa-icons';
import {Button,Card ,CardItem,Input,Spinner, CardList} from './common';
import { connect } from 'react-redux';
import { Change_lang} from './actions';
var customData = require('./string/Language.json');

var data = customData.English


class Language extends Component{

    constructor(props){
        
        super(props);
        this.state ={
            choose:'',
            Language:'',
            x:'row'
        };
    }

  

    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        
        else{
            data=customData. English
            }
        
        if(this.props.reverse){
           this.setState({x:'row-reverse'})
        }    
    }

    componentDidMount() {
       
                this.props.navigation.setParams({done:this.done.bind(this)}); 
                this.props.navigation.setParams({cancel:this.cancel.bind(this)}); 
                this.props.navigation.setParams({ reverse: this.props.reverse });
              }
    
        
              
         
    
    static navigationOptions = ({navigation})=>{
        
        
        
            const {params ={}} =navigation.state;
        
            const headerRight =(
                <TouchableOpacity onPress={params.done}>
                <Text style={{color:'#fff',fontSize:15,fontWeight:'600',padding:10}}>{data.x2}  </Text>
            </TouchableOpacity>
            
                    );
            const headerLeft =(
                    <TouchableOpacity onPress={params.cancel}>
                        <Text style={{color:'#fff',fontSize:15,fontWeight:'600',padding:10}}>{data.x3}</Text>
                </TouchableOpacity>
                    );         
                            
            if(params.reverse){
                return ({headerLeft:headerRight , headerRight:headerLeft ,title:data.x1 });
            }
            else{
                return ({headerRight:headerRight , headerLeft:headerLeft ,title:data.x1 });
            }
            }

            cancel(){
                    this.props.navigation.navigate('Profile')
                }  

            done(){
                
                this.props.Change_lang({ lang:this.state.choose })
                this.props.navigation.navigate('Profile')
                }

            lang_E = ()=>{
                this.setState({check_E:'#FDAC60',check_A:'#fff' }),
                this.setState({choose:'English'})
                
                 }

            lang_A = ()=>{
                    this.setState({check_A:'#FDAC60',check_E:'#fff'}),
                    this.setState({choose:'Arabic'})
                     }   

                 render(){
            
                  var arabic_check=0
                  var english_check=0   
                        AsyncStorage.getItem('language')
                        .then(lang=>{
                            if(lang=='Arabic'){
                            this.setState({Language:lang})
                        }
                       
                    })

                 
                    
                    
                   
                    if (this.state.choose=='English'){
                        english_check=1,arabic_check=0
                    } 
                    else if (this.state.choose=='Arabic'){
                        arabic_check=1,english_check=0
                    }
                    else {
                        //console.log('ssdfgfds '+this.props.lang1)
                        if(this.props.lang1== 'Arabic') {
                            arabic_check=1,english_check=0
                                }
                        else if (this.props.lang1=='English' ) {
                                english_check=1,arabic_check=0
                            }
                            else{
                                console.log('sss '+this.state.Language)
                                if(this.state.Language=='Arabic'){
                                    
                                        arabic_check=1,english_check=0
                                           }
                            
                                else if (this.state.Language=='English' )  {
                                       english_check=1,arabic_check=0
                                      
                                   } 
                                else{
                                    english_check=1,arabic_check=0
                                }   
                            }
                    }
                   // default:return{english_check:1}
                   if(this.props.lang1=='Arabic'){
                    data=customData.Arabic
                }
                else{ AsyncStorage.getItem('language')
                .then(lang=>{
                    if(lang=='Arabic'){
                    data=customData. Arabic
                }
                    else{
                        data=customData. English
                    }
            })
        }
                    
                    
                    return(
                        <ScrollView  style={{backgroundColor:'#eee'}}> 

                        <TouchableOpacity onPress={this.lang_E.bind(this)} style={{flexDirection :this.state.x ,backgroundColor:'#fff',alignItems:'center',marginBottom:3,padding:6}}>
                        <View style={{width:Dimensions.get('window').width-80 ,margin:10}}>
                        <Text style={{ color:'#000',fontSize: 18}}>{data.x4}</Text>
                        <Text>{data.x5}</Text>
                        </View>
                        <Icon name='check'  style={{ color:'#FDAC60',opacity:english_check,fontSize: 30 }} />
                        </TouchableOpacity>
            
                        <TouchableOpacity onPress={this.lang_A.bind(this)} style={{flexDirection :this.state.x ,backgroundColor:'#fff',alignItems:'center',marginBottom:3,padding:6}}>
                        
                        <View style={{width:Dimensions.get('window').width-80 ,margin:10}}>
                        <Text style={{ color:'#000',fontSize: 18}}>{data.x6}</Text>
                        <Text>{data.x7}</Text>
                        </View>
                        <Icon name='check'  style={{ color:'#FDAC60',opacity: arabic_check,fontSize: 30, }} />
                        </TouchableOpacity>
                        
                        </ScrollView>
                    )
                }
}


const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        reverse:state.lang.reverse
        
    }
}

export default connect(mapStateToProps, { Change_lang})(Language)