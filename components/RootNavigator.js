import { StackNavigator } from 'react-navigation';

import loginForm from './LoginForm';
import Splash from './Splash';
import Setting from './Setting';
import SignupForm from './SignupForm';
import addContact from './AddContact';
import EmailVerify from './EmailVerify';
import GetStart from './GetStart';
import ForgetPass from './ForgetPass'
import ChangePass from './ChangePass';
import EditProfile from './EditProfile'
import StreetPal from './StreetPal.js'
import Chat from './Chat';
import Language from './Language';
import Policy from './Policy';
import AboutUs from './AboutUs';
import Safe from  './safe'
import Safe2 from  './safe2'

const RootNavigator = StackNavigator({

    Splash: {
        screen: Splash,
        navigationOptions: {
            header: false,
        }


    },

    loginForm: {
        screen: loginForm,

        navigationOptions: {
            header: false

        }
    },
    ForgetPass: {
        screen: ForgetPass,

        navigationOptions: {
            header: false

        }
    },

    SignupForm: {
        screen: SignupForm,

        navigationOptions: {
            // title: 'Sign Up',
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                height: 50,
            },

            headerTitleStyle: {
                fontSize: 15, fontWeight: 'bold', textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
            },
        }
    },
    EmailVerify: {
        screen: EmailVerify,

        navigationOptions: {
            header: false

        }
    },
    GetStart: {
        screen: GetStart,

        navigationOptions: {
            header: false

        }
    },
    Profile: {

        screen: StreetPal,


        navigationOptions: {


            // title: 'PROFILE',
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                height: 50,
                backgroundColor: '#FDAC60',

            },

            headerTitleStyle: {
                // marginLeft: 60,
                color: '#fff',
                textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                fontSize: 17, fontWeight: '100',

            }
        }
    },

    Setting: {
        screen: Setting,

        navigationOptions: {
            //title: 'OPTIONS',
            tintColor: 'red',
            headerTintColor: 'white',
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                height: 50,
                backgroundColor: '#FDAC60'
            },

            headerTitleStyle: {
                color: '#fff',
                fontSize: 17, fontWeight: '100', textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
            },

            headerLeftStyle: {
                color: '#fff'
            }

        }
    },
    ChangePass: {
        screen: ChangePass,

        navigationOptions: {
            //headerLeft :null,
            //title: 'PASSWORD' ,
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                height: 50,
                backgroundColor: '#FDAC60',

            },

            headerTitleStyle: {
                //marginLeft:60,
                color: '#fff',
                textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                fontSize: 17, fontWeight: '100',

            }
        }

    },
    EditProfile: {
        screen: EditProfile,

        navigationOptions: {
            //headerLeft :null,
            //title: 'EDIT PROFILE' ,
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                height: 50,
                backgroundColor: '#FDAC60',

            },

            headerTitleStyle: {
                //marginLeft:60,
                color: '#fff',
                textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                fontSize: 17, fontWeight: '100',

            }
        }
    },

    addContact: {
        screen: addContact,
        navigationOptions: {
            //title: 'Add contact',
            tintColor: 'red',
            headerTintColor: 'white',
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                height: 50,
                backgroundColor: '#FDAC60'
            },

            headerTitleStyle: {
                color: '#fff',
                fontSize: 17, fontWeight: '100', textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
            },

            headerLeftStyle: {
                color: '#fff'
            }

        }

    },

    Chat: {
        screen: Chat,
        navigationOptions: {
            headerTintColor: 'white',
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                height: 50,
                backgroundColor: '#FDAC60'
            },

            headerTitleStyle: {
                color: '#fff',
                fontSize: 17, fontWeight: '100', textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
            },

            headerLeftStyle: {
                color: '#fff'
            }

        }
    },
    Language: {
        screen: Language,

        navigationOptions: {
            headerTintColor: 'white',
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                height: 50,
                backgroundColor: '#FDAC60'
            },

            headerTitleStyle: {
                color: '#fff',
                fontSize: 17, fontWeight: '100', textAlign: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
            },

            headerLeftStyle: {
                color: '#fff'
            }

        }
    },
    Policy :{
        screen: Policy,
        
                navigationOptions: {
                    headerTintColor: 'white',
                    headerStyle: {
                        elevation: 0,
                        shadowOpacity: 0,
                        height: 50,
                        backgroundColor: '#FDAC60'
                    },
        
                    headerTitleStyle: {
                        color: '#fff',
                        fontSize: 17, fontWeight: '100', textAlign: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                    },
        
                    headerLeftStyle: {
                        color: '#fff'
                    }
        
                }
    },
    AboutUs :{
        screen: AboutUs,
        
                navigationOptions: {
                    headerTintColor: 'white',
                    headerStyle: {
                        elevation: 0,
                        shadowOpacity: 0,
                        height: 50,
                        backgroundColor: '#FDAC60'
                    },
        
                    headerTitleStyle: {
                        color: '#fff',
                        fontSize: 17, fontWeight: '100', textAlign: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                    },
        
                    headerLeftStyle: {
                        color: '#fff'
                    }
        
                }
    },
    Safe :{
        screen: Safe,
        
                navigationOptions: {
                    headerTintColor: 'white',
                    headerLeft :null,
                    headerStyle: {
                        elevation: 0,
                        shadowOpacity: 0,
                        height: 50,
                        backgroundColor: '#FDAC60'
                    },
        
                    headerTitleStyle: {
                        color: '#fff',
                        fontSize: 17, fontWeight: '100', textAlign: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                    },
        
                    headerLeftStyle: {
                        color: '#fff'
                    }
        
                }
    },
    Safe2 :{
        screen: Safe2,
        
                navigationOptions: {
                    headerTintColor: 'white',
                    headerLeft :null,
                    headerStyle: {
                        elevation: 0,
                        shadowOpacity: 0,
                        height: 50,
                        backgroundColor: '#FDAC60'
                    },
        
                    headerTitleStyle: {
                        color: '#fff',
                        fontSize: 17, fontWeight: '100', textAlign: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                    },
        
                    headerLeftStyle: {
                        color: '#fff'
                    }
        
                }
    },

}, {
        initialRouteName: 'Splash',
        headerMode: 'screen'
    });




export default RootNavigator;
