import React ,{Component} from 'react';
import {StyleSheet,View,Text,Image,Animated,Easing,Dimensions, TouchableHighlight, TouchableOpacity} from 'react-native';


 export default class ChatItem extends Component{
     state={
        initalPosition: new Animated.Value(-1*Dimensions.get('window').width),
        condition :true
     }
     componentDidMount() {
        
           Animated.timing(                          // Base: spring, decay, timing
               this.state.initalPosition,                 // Animate `bounceValue`
               { 
                 toValue: (0), 
                 duration:700,                   
                 friction : 1, 
                 easing :Easing.linear                      
               }
             )  .start();                              // Start the animation
     }

     
   
     render(){
        
        if(this.props.id==1){
            return ( 
                
                <View style={[styles.qq,
                //{transform: [{translateX: this.state.initalPosition}] }
                ] }  onPress ={this.props.onpress} >

                    <Image source={require('../photo/s.png')}  style={styles.q}/>

                  
                        <TouchableHighlight onPress ={this.props.onpress}  style={[styles.streetpal]}>
                        <Text style={[styles.text,]} >{this.props.message} </Text>
                        </TouchableHighlight>
                    
                </View>
                )
            }

        return(
            <View style={[styles.qq,{justifyContent: 'flex-end',}]}>
            
                           
            <TouchableHighlight style={styles.me}>
                <Text style={styles.text}>{this.props.message}</Text>
            </TouchableHighlight>
            </View>
            
        )

}
}


const styles = StyleSheet.create({
   
    q:{
        width:30, height:30, 
        borderRadius : 15,

    },
    qq:{
        flexDirection: 'row',
        padding:5,
        marginTop:3,
     
    
    },
    text:{
        
        //fontWeight:'500',
        //fontSize:14,
        color:'#fff',
        
    },
    me:{
        backgroundColor:'#FDAC60',
        borderRadius : 15,
        borderBottomRightRadius:5,
        padding:10,
        
        
    },
    streetpal:{
        backgroundColor:'#2f424c',
        //justifyContent : 'center',
        //alignItems : 'center',
        borderRadius : 15,
        padding:10,
        borderTopLeftRadius:5,
        marginLeft:5,
        marginRight:30
       // flex:1
      
    },
  
   
})

export {ChatItem };