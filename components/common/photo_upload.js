import {Text,StyleSheet,View, Image, TouchableOpacity} from 'react-native';
import React, { Component } from 'react'
//import RNLanguages from 'react-native-languages';


const Photo_upload = (props)=>{  
    
   
    return(
        <View style={styles.upload}>

            <TouchableOpacity onPress={props.onPress}>
                
                { props.avatarSource === null ?
                    
                    <View style={{backgroundColor:'#FDAC60',padding:20,borderRadius:48,borderWidth:8,borderColor:'rgba(249, 176, 76,.3)'}}>
                        <Image source={require('../photo/user2.png')} style={styles.image}/>
                    </View> :
                    <Image style={styles.avatar} source={props.avatarSource} />
                }
                    

            </TouchableOpacity>   

            
        </View >
    );
}



const styles=StyleSheet.create({

    image :{
        height:40,
        width :40, 
        
    },
    upload:{
        
        justifyContent : 'center',
        alignItems : 'center',
    },
    avatar: {
        borderRadius: 50,
        width: 100,
        height: 100
      }
   
    
});




export {Photo_upload};