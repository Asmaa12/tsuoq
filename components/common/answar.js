import React from 'react';
import {StyleSheet,View,Text} from 'react-native';


const Answar = (props)=>{
   
    return(
        <View style={styles.answar}>
                <Text style={styles.text}>
                    {props.children[0]}
                </Text>
        </View>
    );
};
const styles = StyleSheet.create({
    text:{
        
        fontWeight:'500',
        fontSize:14,
        color:'#000'
    },
   answar:{
        backgroundColor:'#eee',
        margin:30,
        alignItems : 'center',
        padding:20,
    },
   
    
    
  });
export {Answar};