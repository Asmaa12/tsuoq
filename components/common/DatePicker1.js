var React = require('react');
var ReactNative = require('react-native');
var {
  View,
  DatePickerAndroid,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  AppRegistry,Image
} = ReactNative;

const MyDatePicker1 = (props)=>{
   
    return (
      <View>
        <View>
          <TouchableWithoutFeedback
            style={{width:300,borderWidth:1,justifyContent:'center',alignItems:'center'}}
            onPress={props.onPress}>
            <View style={{flexDirection:'row',margin:10}}>
              <Text style={styles.text}>{props.simpleText}</Text>
              <Image source={require('../photo/date.png')} style={{width:30,height:30}} />
            </View>
          </TouchableWithoutFeedback>
        </View>
       
      </View>
    );
  
}

var styles = StyleSheet.create({
  text: {
    color: 'rgba(0,0,0,.5)',
    
  },
});

export { MyDatePicker1};