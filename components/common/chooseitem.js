import React ,{Component} from 'react';
import {TouchableOpacity,Text,StyleSheet,Animated,Dimensions,Easing} from 'react-native';


export default class Choose extends Component{
    state={
       initalPosition: new Animated.Value(-1*Dimensions.get('window').width),
       condition :true
    }
    componentDidMount() {
       
          Animated.timing(                          // Base: spring, decay, timing
              this.state.initalPosition,                 // Animate `bounceValue`
              { 
                toValue: (0), 
                duration:700,                   
                friction : 1, 
                easing :Easing.linear                      
              }
            )  .start();                              // Start the animation
    }
    render(){
    return(
        <TouchableOpacity 
        style={[styles.button,
            { transform: [{translateX:-Dimensions.get('window').width}] ,transform: [{translateX: this.state.initalPosition}],}
            ] }
            onPress ={this.props.onpress}>
            <Text style ={styles.tx}>
                {this.props.children}
            </Text>
        </TouchableOpacity>  
              
    );
}
}
const styles = StyleSheet.create({
    button :{
        
        margin : 5,
        marginLeft:10,
        marginRight:10,
        borderRadius : 50 ,
        borderWidth:1,
        borderColor : '#FDAC60' , 
        alignItems :'center', 
        backgroundColor:'#fff',
        
    },
    tx :{
        padding:5,
        paddingLeft:15,paddingRight:15,
        fontSize : 14 ,
        fontWeight :'300',
    }
});
export {Choose};
