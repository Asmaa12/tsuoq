import React from 'react';
import {StyleSheet,View} from 'react-native';


const CardItem = (props)=>{
    return(
        <View style ={styles.carditem}>
            {props.children}
        </View>
    );
};

const styles = StyleSheet.create({
    carditem :{
        
        padding :5,
        borderRadius :5,
        marginTop:2,
        marginLeft:30,
        marginRight:30,
        //borderBottomWidth :2 , 
        //borderColor :'#eee',
        backgroundColor : '#eee',
        justifyContent :'flex-start',
        flexDirection :'row',
        flex :1,
        
    }
});

export { CardItem};