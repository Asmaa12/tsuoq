import React from 'react';
import {TouchableOpacity,Text,StyleSheet} from 'react-native';



const Button = (props) =>{
    return(
        <TouchableOpacity style ={[styles.button,{ backgroundColor : props.color ,
            height : props.height }]} onPress ={props.onpress}>
            <Text style ={styles.tx}>
                {props.children}
            </Text>
        </TouchableOpacity>        
    );
}

const styles = StyleSheet.create({
    button :{
       
        marginRight : 40,
        marginLeft : 40,
        marginTop : 10,
        borderRadius : 50 ,
        justifyContent : 'center', alignItems :'center',
        flex :1,
        borderBottomWidth :3 , 
        borderColor :'#eee',
    },
    tx :{
        color :'#fff',
        fontSize : 15 , fontWeight :'bold'
    }
});
export {Button};