import React from 'react';
import {StyleSheet,View,Text,TouchableOpacity} from 'react-native';
//import RNLanguages from 'react-native-languages';

const Option1 = (props)=>{
    
var reverse='row'
if(props.reverse){
    reverse='row-reverse'  
}
    return(

                <View>
                    
                    <TouchableOpacity onPress={props.onpress} style={{ flexDirection :reverse ,backgroundColor:'#fff',direction:'rtl'}}>
                        <Text style={styles.item}>{props.children}</Text>  
                        <Text style={styles.option}> {props.vector} </Text>
                    </TouchableOpacity> 
                        
                </View>

    )
}


const styles=StyleSheet.create({
    contt:{
        padding: 10,
        color : '#999',
        fontSize : 16 ,
        fontWeight: '100'
    },
   
    option:{
        fontSize : 14 ,
        fontWeight :'100',
        color : '#777',
        padding: 10,
        position:'absolute',
        right:17,
       // backgroundColor : '#fff',
    },
    item:{
       // backgroundColor : '#fff',
        color : '#777',
        fontSize : 14 ,
        fontWeight :'bold',
        padding: 10,
        paddingLeft:17,
        
    },
})


export { Option1};