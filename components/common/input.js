import React from 'react';
import { StyleSheet, View, Text, TextInput, Image } from 'react-native';


const Input = (props) => {

    if (props.error) {
        if (props.error != '') {
            // <Text style ={styles.error}>{props.error}</Text>  
            return (
                <View style={styles.container}>

                    <View>
                        <TextInput style={[styles.input, { borderColor: 'red', borderBottomWidth: 1, borderRadius: 4, writingDirection: 'rtl' }]}
                            placeholderTextColor='rgba(255,0,0,.4)'
                            placeholder={props.error}
                            secureTextEntry={props.securetext}
                            onChangeText={props.onChangeText}
                            autoCorrect={false}
                            autoFocus={props.autoFocus}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            editable={props.editable}
                            keyboardType={props.keyboardType}
                            onFocus={props.onFocus}
                            autoCapitalize={'none'}
                            value={props.value}
                            returnKeyType={"next"}
                            multiline={props.multiline === false ? false : true}
                        />
                    </View>



                </View>


            )
        }
    }

    return (
        <View style={styles.container}>
            <TextInput style={styles.input}
                placeholder={props.placeholder}
                secureTextEntry={props.securetext}
                onChangeText={props.onChangeText}
                autoCorrect={false}
                placeholderTextColor='#aaa'
                autoFocus={props.autoFocus}
                underlineColorAndroid='rgba(0,0,0,0)'
                editable={props.editable}
                keyboardType={props.keyboardType}
                onFocus={props.onFocus}
                returnKeyType={"next"}
                value={props.value}
                autoCapitalize={'none'}
                multiline={props.multiline === false ? false : true}
            />


        </View>
    )
}

const styles = StyleSheet.create({
    error: {
        color: 'red',
        textAlign: 'center',
    },
    container: {

        flex: 1,

    },

    input: {
        flex: 1,
        fontSize: 16,
        color: '#000',
        paddingLeft: 15,
        paddingRight: 15,


    },
});

//export componant for other apps
export { Input };
