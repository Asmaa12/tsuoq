import React from 'react';
import {StyleSheet,View} from 'react-native';


const Card = (props)=>{
    return(
        <View style ={styles.cardstyle}>
            {props.children}
        </View>
    );
};

const styles = StyleSheet.create({
    cardstyle :{
       
        
        //position :'relative',
        //top: 0, left:0 ,right:0,
        marginLeft : 30,  
        marginRight : 30,
        marginTop : 40,
        padding: 10,
       backgroundColor:'#fff',
       height:140,
       width:'100%',
       justifyContent :'flex-start',
       flexDirection :'column',
       //flex :1,
   


   
        //borderWidth :1 ,
        //borderRadius :5 ,
        //borderColor :'rgba(45,89,90,0)',
        //shadowColor : '#000',  
        //shadowOpacity : 0.3
    }
});

export { Card};