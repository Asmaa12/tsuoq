import React from 'react';
import {StyleSheet,View,Text} from 'react-native';


const Title = (props)=>{
    return(
        <View>
            <Text style={styles.title}>
                {props.children}
            </Text>
        </View>
    );
};


const styles=StyleSheet.create({
    title:{
        
        padding: 10,
        color : '#999',
        fontSize : 16 ,
        fontWeight: '100'
    },
})

export { Title};