import React from 'react';
import {StyleSheet,View} from 'react-native';


const CardList = (props)=>{
    return(
        <View style={{backgroundColor:'#fff',}}>
            {props.children}
        </View>
    );
};

export { CardList};