import React from 'react';
import { View,ActivityIndicator,StyleSheet,Text,Dimensions} from 'react-native';


const Loading =(props)=>{

    return (
        <View style={styles.loading}>
          <View style={{backgroundColor:'#fff',flexDirection:'row',height:120,width:(Dimensions.get('window').width-40)
          ,justifyContent: 'center',
          alignItems: 'center'}}>
          <ActivityIndicator size={ 'large'} style={{padding:20}} />
          <Text>{props.children}</Text>
          </View>
        </View>
    );
}

const styles =StyleSheet.create({
    loading: {
        zIndex:3,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        //opacity: 0.5,
        backgroundColor:'rgba(0,0,0,.5)',
        justifyContent: 'center',
        alignItems: 'center',
    }
        
    });

export {Loading}