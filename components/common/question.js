import React from 'react';
import {StyleSheet,View,Text,Image} from 'react-native';


const Question = (props)=>{
    return(

                <View style={styles.qq}>
                    <Image source={require('../photo/q.png')}  style={styles.q}/>
                    <View style={styles.text1}>
                    <Text style={styles.text}>{props.children}</Text>
                    </View>
                </View>

    )
}


const styles = StyleSheet.create({
   
    q:{
        width:50, height:50, 
        

    },
    qq:{
        flexDirection: 'row',
        backgroundColor:'#eee',
        marginRight:30,
        marginLeft:30,
        alignItems : 'center',
        padding:20,
    },
    text:{
        
        fontWeight:'500',
        fontSize:14,
        color:'#000'
    },
    text1:{
        justifyContent : 'center',
        alignItems : 'center',
        marginRight:40,
        marginLeft:20,
    },
})

export { Question};