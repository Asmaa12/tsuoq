import React from 'react';
import { StyleSheet, View, Text, TextInput, Image } from 'react-native';


const Input2 = (props) => {



    return (
        <View style={{ borderWidth: 0.5, margin: 10, paddingLeft: 10 }}>
            <TextInput
                style={{ width: 250, }}
                value={props.value}
                placeholder={props.placeholder}
                placeholderTextColor='#aaa'
                keyboardType={props.keyboardType}
                autoFocus={props.autoFocus}
                editable={props.editable}
                secureTextEntry={props.securetext}
                underlineColorAndroid='rgba(0,0,0,0)'
                onChangeText={props.onChangeText}
                multiline={true} />
        </View>)
}

export { Input2 };
