import React, { Component } from 'react'
import DatePicker from 'react-native-datepicker'
//import RNLanguages from 'react-native-languages';

const MyDatePicker = (props)=>{
    if(props.error!=''){
        return (
            <DatePicker
              style={{width: 250}}
              date={props.date}
              mode="date"
              placeholder={props.error}
              format="YYYY-MM-DD"
              minDate="1950-05-01"
              maxDate="2016-06-01"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                  dateIcon: {
                      width:30,height:20,
                      margin:0,
                  },
                  dateInput: {
                      position: 'absolute',
                      left: 12,
                      top: 0,
                      borderWidth:0,
                      borderBottomWidth:1,
                      borderBottomColor:'red'
                  }
                  
              }}
              onDateChange={props.onDateChange}
            />
          )
    }
 
  else{
   

    return (
        <DatePicker
            style={{width: 250}}
            date={props.date}
            mode="date"
            placeholder="Birth Date"
            format="YYYY-MM-DD"
            minDate="1950-05-01"
            maxDate="2016-06-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
                dateIcon: {
                    width:30,height:20,
                    margin:0,
                },
                dateInput: {
                    position: 'absolute',
                    left: 12,
                    top: 0,
                    borderWidth:0,
        
                }
                
            }}
            onDateChange={props.onDateChange}
        />
        )

}



}


export { MyDatePicker};