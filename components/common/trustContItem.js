import React from 'react';
import {StyleSheet,View,Text,Image,TouchableOpacity,Dimensions} from 'react-native';
import Icon from 'react-native-fa-icons';

const TrustItem = (props)=>{
    const colors=['#2E6E6B','#3A6E2E','#2E336E','#6B2E6E','#93374F','#9FA537','#710142','#C8425E','#76C26C','#BE6CC2','#6C84C2']
    //<Image source={require('../photo/user2.png')} style={styles.image}/>
    return (
        <View style ={styles.contactItem}>
            <View style ={{width:Dimensions.get('window').width-100,flexDirection: 'row',}}>

            <View style={styles.x}>
            <Icon name='user-circle'  style={{ color:colors[Math.floor((Math.random() * 11) )],fontSize: 35, }} />
            </View>
            <View>
                <Text style={styles.name}>  {props.Name}  </Text >
                <Text style={styles.name}>{props.phoneNumber}</Text>
            </View>
            </View>

            <TouchableOpacity onPress={props.onPress} >
                <Image source={require('../photo/delete.png')} style={[styles.image]}/>
            </TouchableOpacity>

        </View>)
};


const styles=StyleSheet.create({
    name:{
        
        paddingLeft:10,
        color : '#999',
        fontSize : 16 ,
        fontWeight: '400'
    },
    contactItem:{
    
        flex :1,
        backgroundColor:'#fff',
        flexDirection: 'row',
        alignItems : 'center',
        padding:10,
        borderTopWidth :2 , 
        borderColor :'#eee',
    },
    image:{
     
     width:30, height:30,
    },
    x:{

        alignItems : 'center',justifyContent:'center'
    }
})

export { TrustItem};