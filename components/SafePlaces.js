
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,Dimensions,Image, TouchableOpacity,Alert,Linking,ActivityIndicator
} from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import axios from 'axios';
import { Loading} from './common/index';
import { connect } from 'react-redux';
var customData = require('./string/SafePlaces.json');

var data = customData.English

const { width, height}=Dimensions.get('window')
const ASPECT_RATIO = (width/height)
const LATTITUDE_DELTA = 0.01822
const LONGTITUTE_DELTA = LATTITUDE_DELTA *(width/height)

class SafePlaces extends Component<{}> {
 // static navigationOptions = { };

  constructor(){
    super();
    this.state ={
        initialPosition: {
          latitude: 0,
          longitude: 0,
          latitudeDelta: 0,
          longitudeDelta: 0,
        },

        markerPosition :{
          latitude: 0,
          longitude: 0,
        },

        latitude_police:0,
        longitude_police:0,

        longitude_hospital:0,
        latitude_hospital:0,
      
        d1:null,
        d2:null,

        click:false,
        progressVisible:false,
        
    
    }
  }

  componentWillMount(){
    if(this.props.lang1=='Arabic'){
        data=customData.Arabic
    }
    else{
        data=customData. English
    
        }
}

  watchID : ?number =null
  componentWillUnmount () {
    navigator.geolocation.clearWatch(this.watchID)
  }
  componentDidMount(){
    navigator.geolocation.getCurrentPosition((position) =>{
      var lat = parseFloat(position.coords.latitude)
      var long = parseFloat(position.coords.longitude)

      var initialRegion ={
        latitude : lat,
        longitude : long,
        latitudeDelta : LATTITUDE_DELTA,
        longitudeDelta : LONGTITUTE_DELTA
      }
      this.setState({initialPosition: initialRegion})
      this.setState({markerPosition: initialRegion})
    },

    {enableHighAccuracy: true, timeout:20000 , maximumAge:1000})

    this.watchID = navigator.geolocation.watchPosition((position) => {
      // Create the object to update this.state.mapRegion through the onRegionChange function
    var lat =parseFloat(position.coords.latitude)
    var long = parseFloat (position.coords.longitude)

    var lastRegion ={
      latitude : lat ,
      longitude : long,
      longitudeDelta : LONGTITUTE_DELTA,
      latitudeDelta : LATTITUDE_DELTA,
    }

    this.setState({initialPosition: lastRegion})
    this.setState({markerPosition: lastRegion})
    
    
    }
    
    );

  }

  

 GetNearestHospital=()=>{
 
 
   this.setState({progressVisible:true}),
  axios.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+ this.state.initialPosition.latitude+','+this.state.initialPosition.longitude+'&rankby=distance&types=hospital&key=AIzaSyBHTlp76RyPgdtiHr7Aq9ULX69EfyomkqI')
   .then(result=>{
     
     console.log(result.data.results[0].geometry.location.lat),
     console.log(result.data.results[0].geometry.location.lng),
     
    this.setState({latitude_hospital:result.data.results[0].geometry.location.lat , longitude_hospital:result.data.results[0].geometry.location.lng});
    this.setState({progressVisible:false}),
    Alert.alert(
      ' ',
      data.x1,
      [
          {text: data.x2 , onPress: ()=>console.log('cancel')},
          {text: data.x3, onPress: ()=>{
            Linking.openURL('https://www.google.es/maps/dir/'+"'"+this.state.initialPosition.latitude+','+this.state.initialPosition.longitude+ "'/'"+result.data.results[0].geometry.location.lat+','+result.data.results[0].geometry.location.lng+"'" )
          }}
      ],
      {cancelable: false}
  )
    
  
  })
   .catch(err=>{
    this.setState({progressVisible:false}),
     console.log(err)})
 
  }


  GetNearestPolice=()=>{
    
   
    this.setState({progressVisible:true})
    axios.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+ this.state.initialPosition.latitude+','+this.state.initialPosition.longitude+'&rankby=distance&types=police&key=AIzaSyBHTlp76RyPgdtiHr7Aq9ULX69EfyomkqI')
     .then(result=>{
    
       console.log(result.data.results[0].geometry.location.lat),
       console.log(result.data.results[0].geometry.location.lng),
       
      this.setState({latitude_police:result.data.results[0].geometry.location.lat , longitude_police:result.data.results[0].geometry.location.lng});
      this.setState({progressVisible:false}),
      Alert.alert(
        ' ',
          data.x4,
        [
            {text: data.x2 , onPress: ()=>console.log('cancel')},
            {text: data.x3, onPress: ()=>{
              Linking.openURL('https://www.google.es/maps/dir/'+"'"+this.state.initialPosition.latitude+','+this.state.initialPosition.longitude+ "'/'"+result.data.results[0].geometry.location.lat+','+result.data.results[0].geometry.location.lng+"'" )
            }}
        ],
        {cancelable: false}
    )
    
    })
     .catch(err=>{
      this.setState({progressVisible:false}),
       console.log(err)})
   
    }

    load=()=>{
      
      if(this.state.progressVisible){
        return (
          <Loading>{data.x5}</Loading>
       )
      }

    }
  
  render() {
    
    
    
    return (
      
      <View style={styles.container}>
          <MapView
          style={styles.map}
          region={
            this.state.initialPosition
          }
          showsUserLocation={true}
          followUserLocation={true}
          minZoomLevel={1}
        >
         

          <MapView.Marker
             //pinColor ='#174442'
             //title ='Police Station'
             image={require('./photo/marker_police_30.png')}
            coordinate={{latitude:this.state.latitude_police, longitude:this.state.longitude_police}}>
             
          </MapView.Marker> 


        <MapView.Marker
             //pinColor ='red'
             //title ='Hospital'
             image={require('./photo/marker_hospital_30.png')}
            coordinate={{latitude:this.state.latitude_hospital, longitude:this.state.longitude_hospital}}>
             
          </MapView.Marker>
          <MapView.Marker
            //pinColor ='#FDAC60'
            //title ='You'
           image={require('./photo/guide1.png')}
            coordinate={this.state.initialPosition}>
              
          </MapView.Marker> 

          <MapViewDirections
          origin={this.state.initialPosition}
          destination= {{latitude: this.state.latitude_hospital, longitude: this.state.longitude_hospital}}
          apikey={'AIzaSyBHTlp76RyPgdtiHr7Aq9ULX69EfyomkqI'}
          strokeWidth={5}
          strokeColor="#FDAC60"
        />
         <MapViewDirections
          origin={this.state.initialPosition}
          destination= {{latitude: this.state.latitude_police, longitude: this.state.longitude_police}}
          apikey={'AIzaSyBHTlp76RyPgdtiHr7Aq9ULX69EfyomkqI'}
          strokeWidth={5}
          strokeColor="#FDAC60"
        /> 
          
            
        </MapView>
      

        <TouchableOpacity  style ={styles.image1}
          onPress={this.GetNearestPolice.bind(this)}>
          <Image source={require('./photo/police.jpg')} style={ {height:80,width :80,borderRadius :40}}/>
        </TouchableOpacity>

        <TouchableOpacity  style ={styles.image2}
          onPress={this.GetNearestHospital.bind(this)}>
          <Image source={require('./photo/hospital.png')} style={ {height:80,width :80,borderRadius :40}}/>
        </TouchableOpacity>

        {this.load()}
      </View>
    );
  



}
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex:-1
  },
  icon:{
    width:20, height:23
},  
image1 :{

  position:'absolute',
  top:20,
  left:20,
  zIndex:2,

},
image2 :{
  
    position:'absolute',
    top:20,
    right:20,
    zIndex:2,
  
  },

});

const mapStateToProps = state => {
  return {
      lang1: state.lang.language,
      
  }
}

export default connect(mapStateToProps,{ })(SafePlaces);
