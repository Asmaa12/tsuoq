import React ,{Component} from 'react';
import {Text,StyleSheet,View, ColorPropType, Image,Animated, TouchableOpacity, AsyncStorage, TextInput,ScrollView} from 'react-native';
import axios from 'axios';
import { connect } from 'react-redux';

import {Button,Card ,CardItem,Input,Spinner, CardList,Input2 } from './common';
var customData = require('./string/ForgetPass.json');

var data = customData.English

var i=0,x=0

class ForgetPass extends Component {
    
    
      constructor(props){
        super(props);
        this.state ={
            error: '',
            email: '',
            emailSent: false,
            loading:false,
            password:'',
            confirmPassword:'',
            user:'',
            code: '',
            success: false
        };
    }
    
    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{
            data=customData. English
        
            }
    }
    
    send=()=>{
      
        this.setState({error:''})
        this.setState({loading:true});

        if(this.state.emailSent){
            let user = this.state.user
            let code = this.state.code
            let pass = this.state.password
            let confirmPassword = this.state.confirmPassword
            
        
            if(code==''){
                
                
                return( this.setState({error: data.x1}),this.setState({loading:false}))
            }

            else if(user==''){
                
                return( this.setState({error:data.x2}),this.setState({loading:false}))
            }

            else if(pass==''){
                
               
                return( this.setState({error:data.x3}),this.setState({loading:false}))
            }

            else if(confirmPassword==''){
                
               
                return( this.setState({error:data.x4}),this.setState({loading:false}))
            }
             //check password
            else if(!( (this.state.password.toUpperCase() != this.state.password)&&
             (this.state.password.toLowerCase()!=this.state.password)&&
             (/\d/.test(this.state.password)) &&(this.state.password.length>8))){

                
               
                 return(
                 this.setState({error:data.x5}),
                 this.setState({password:''}),
                 this.setState({loading:false})
                 )
             }

             else if(pass != confirmPassword){
                
               
                 return(
                this.setState({error:data.x6}),
                this.setState({confirmPassword:''}),
                this.setState({loading:false})
                 )
            }
            
             else{
                
                 return(
            axios.post('https://streetpal.org/api/me/pass-reset/respond',{user,code,pass})
            .then(resp=>{
                //console.log('ok') 
                this.setState({success:true}) })
            .catch(err=>{
                //console.log(err.response.data),
            
                
                this.setState({error:data.x7}) ,this.setState({loading:false})
                //this.setState({code:''},{user:''},{password:''},{confirmPassword:''}) 
        
                })
             )
            }
        }
        
        else{
            let email= this.state.email

            //this.validate({email: {email: true,required: true} });
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(re.test(email)){
                
                axios.post('https://streetpal.org/api/me/pass-reset/request',{email})
                .then(resp=>{
                    //console.log(resp.data) ,
                    this.setState({emailSent:true}),
                    this.setState({error:''}),
                    this.setState({loading:false})
                  
                })
                .catch(err=>{
                    
                
                    this.setState({error:data.x7}),this.setState({loading:false})})
            
            }
            else{
                
                this.setState({error:data.x8})  
                this.setState({email:''}) 
                this.setState({loading:false})
            }
        }

    }
    cancel=()=>{
        
        this.props.navigation.navigate('loginForm');
        
    }
    
    start(){
        this.props.navigation.navigate('loginForm');
     }
    
    renderButtons=()=>{
        if(this.state.loading){

            return (
                <View style={{marginTop:30}}>
                    <Spinner/>
                </View>)
        }
        else{
            
           
            
        return(
            <View style={{marginTop:10}}>
                <Button height ={50} color={'#FDAC60'} onpress={this.send.bind(this)} >{data.x9}</Button>
                <Button height ={50} color={'#293030'} onpress={this.cancel.bind(this)}>{data.x10}</Button>
            </View>
        )
    }
    };
    render(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{ AsyncStorage.getItem('language')
        .then(lang=>{
            if(lang=='Arabic'){
            data=customData. Arabic
        }
            else{
                data=customData. English
            }
    })
}
        if(this.state.success){
            

            return(
                <ScrollView style={styles.cont}>
                
                <View style={{flex:1, justifyContent : 'center',alignItems : 'center',}}>
                <Image source={require('./photo/correct.png')} style={styles.image}/>
                <Text style={styles.contt}>{data.x12}</Text>
                </ View>
                <Button height ={52} color={'#FDAC60'} onpress ={this.start.bind(this)}>{data.x11}</Button>
                
            </ScrollView>
            )
        }

        else{
            if(this.state.emailSent){
                
               
                return( 
                <ScrollView style={styles.cont}>
                    <View style={{justifyContent : 'center', alignItems : 'center',}}>
                    <Image source={require('./photo/Capture2.jpg')} style={styles.logo2}/>
                    <Text style={styles.text2}> {data.x13}</Text>
                    
                    
                    <CardList>
                    
                    <Input2 
                        placeholder ={data.x14} 
                        autoFocus={true}
                        value={this.state.code}
                        editable ={!(this.props.loading)}
                        onChangeText ={(code)=> this.setState({code})}/>
                        

                    <Input2
                        placeholder ={data.x15} 
                        value={this.state.user}
                        editable ={!(this.props.loading)}
                        onChangeText ={(user)=> this.setState({user})}/> 

                    <Input2   
                        placeholder ={data.x16} 
                        editable ={!(this.props.loading)}
                        value={this.state.password}
                        securetext = {true}
                        onChangeText ={(password)=> this.setState({password})}/> 

                    <Input2   
                        placeholder ={data.x17} 
                        editable ={!(this.props.loading)}
                        value={this.state.confirmPassword}
                        securetext = {true}
                        onChangeText ={(confirmPassword)=> this.setState({confirmPassword})}/> 

                    </CardList>
                    <Text style ={{color:'red',textAlign:'center',marginBottom:5 ,fontSize:12}}>{this.state.error}</Text> 
                    </View>
                    {this.renderButtons()}
                </ScrollView>)
            }

            
            
               
            return(
                <ScrollView style={styles.cont}>
                    <View style={{justifyContent : 'center', alignItems : 'center',}}>
                    <Image source={require('./photo/Capture.jpg')} style={styles.logo}/>
                    <Text style={styles.text}>{data.x18}</Text>
                    <Text style={styles.text2}>{data.x19}</Text>
                    
                    
                    <CardList>
                        <Input2
                        value={this.state.email}
                        placeholder ={data.x20} 
                        ref="email"
                        keyboardType = 'email-address'
                        autoFocus={true}
                        editable ={!(this.props.loading)}
                        onChangeText ={(email)=> this.setState({email})}/> 
                        
                    </CardList>
                    <Text style ={{color:'red',textAlign:'center',marginBottom:5}}>{this.state.error}</Text> 
                    </View>
                    {this.renderButtons()}
                </ScrollView>
                )
            
            }
    }
}

const styles=StyleSheet.create({
    logo:{
        width:200,
        height:90,
        marginTop:40 
    },
    logo2:{
        width:110,
        height:140,
        marginTop:20 
    },
    cont:{
        
        backgroundColor:'#fff',
        flex:1
    },
    icon:{
        width:27, height:23
    },
    text :{
        padding: 10,
        margin : 30,
       fontSize:25,
       fontWeight:'300',
       color:'#000'
    },
    text2:{
        textAlign: 'center',
        fontWeight:'500',
        fontSize:14,
        padding:20,
        color:'#999'
    },
    contt:{
        
            padding: 10,
            margin : 30,
           fontSize:25,
           fontWeight:'300',
           color:'#000'
        },
       
        image :{
            height:100,
            width :100,
            marginTop:80, 
        
        },
        cont:{
           
            backgroundColor:'#fff', 
        
        },
})


const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        
    }
}

export default connect(mapStateToProps, { })(ForgetPass);


