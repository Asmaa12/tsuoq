import React, { Component } from 'react';
import {AppRegistry,ScrollView,AsyncStorage} from 'react-native';
import Swiper from 'react-native-swiper';
//import RNLanguages from 'react-native-languages';

import { Question, Answar } from './common/index';
import { connect } from 'react-redux';
import { Change_lang} from './actions';

var customData = require('./string/TabLib.json');

var data = customData.English




class TabLib extends Component {
   
        state ={
            x:''
        }
    
    
    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
            this.setState({x:'g'})
        }
        else{
            data=customData. English
            this.setState({x:'g'})
        }
    

    }
  render(){
   
    return (
        
      <Swiper loop={false} activeDotColor='#000' 
      paginationStyle={{backgroundColor:'#fff',position:'absolute',bottom:0,left:0,padding:10}}>
        
        <ScrollView style={{backgroundColor:'#fff',}}> 
            <Question>{data.q1}</Question>
            <Answar>{data.a1} </Answar>
        </ScrollView>

        <ScrollView style={{backgroundColor:'#fff',}}> 
            <Question>{data.q2}</Question>
            <Answar>{data.a2} </Answar>
        </ScrollView>

        <ScrollView style={{backgroundColor:'#fff',}}> 
            <Question>{data.q3}</Question>
            <Answar>{data.a3} </Answar>
        </ScrollView>

        <ScrollView style={{backgroundColor:'#fff',}}> 
            <Question>{data.q4}</Question>
            <Answar>{data.a4} </Answar>
        </ScrollView>

        <ScrollView style={{backgroundColor:'#fff',}}> 
            <Question>{data.q5}</Question>
            <Answar>{data.a5} </Answar>
        </ScrollView>

        <ScrollView style={{backgroundColor:'#fff',}}> 
            <Question>{data.q6}</Question>
            <Answar>{data.a6} </Answar>
        </ScrollView>
      </Swiper>
      
    )
}
}


const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        
    }
}

export default connect(mapStateToProps, { Change_lang})(TabLib);