import React ,{Component} from 'react';
import {Text,StyleSheet,View, ColorPropType, Image,Animated, TouchableOpacity, AsyncStorage, TextInput,ScrollView} from 'react-native';
import axios from 'axios';
import { connect } from 'react-redux';

import { Button,Card ,CardItem,Input,Spinner, CardList } from './common';
import { email_verify } from './actions'
var customData = require('./string/EmailVerify.json');

var data = customData.English
var i=0,x=0

class EmailVerify extends Component{
    
    
      constructor(){
        super();
        this.state ={
            username: '',
            code: '',
            sec:0,
            min:0
        };
    }

    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{
            data=customData. English
        
            }
    }

    componentWillUnmount(){
        this.counter()
        AsyncStorage.getItem('username')
        .then(username=>this.setState({username :  username }))
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.valid){
            this.props.navigation.navigate('GetStart');
        }
    }

    componentDidMount() {
       this.counter()
       AsyncStorage.getItem('username')
       .then(username=>this.setState({username :  username }))
    }

    counter=()=>{
        setTimeout(()=>{
            
            i++;
            this.setState({sec:i});
            
            if(i<60){
                this.counter();
            }
            else{
                i=0;
                x++;
                this.setState({min:x}); 
                this.setState({sec:i});
                this.counter();

            }
          },1000)
        }

 
    resend(){
        i=0,x=0
        
        AsyncStorage.getItem('app_token')
        .then(token=>
            axios.post('https://streetpal.org/api/me/email-verify/request',{},{ 'headers': { 'Authorization': 'Bearer ' + token } })
            .then(resp=>{console.log('sent') })
            .catch(err=>{console.log('error')})
        )
    }
    submit=()=>{
        
        let code = this.state.code;
        
        this.props.email_verify({code });
    }
    
    renderButtons=()=>{
        if(this.props.loading){

            return (
                <View style={{marginTop:30}}>
                    <Spinner/>
                </View>)
        }
        var x1= 'Submit'
        var x2='Resend'
        
        return(
            <View style={{marginTop:30}}>
                <Button height ={40} color={'#FDAC60'} onpress={this.submit.bind(this)} >{data.x1}</Button>
                <Button height ={40} color={'#293030'} onpress={this.resend.bind(this)}>{data.x2}</Button>
            </View>
        )
    };
    render(){
    
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{ AsyncStorage.getItem('language')
        .then(lang=>{
            if(lang=='Arabic'){
            data=customData. Arabic
        }
            else{
                data=customData. English
            }
    })
}
        return(
            <ScrollView style={styles.cont}>
                <View style={{justifyContent : 'center', alignItems : 'center',}}>
                <Image source={require('./photo/s.png')} style={styles.logo}/>
                <Text style={styles.text}>{data.x3} {this.state.username} !</Text>
                <Text style={styles.text2}>{data.x4}</Text>
                
                <Text style={styles.text2}>{this.state.min} : {this.state.sec}</Text>
                <CardList>
                <TextInput  style={{width:250}} 
                    placeholder ={data.x5} 
                    autoFocus={true}
                    onChangeText ={(code)=> this.setState({code})}/> 
                </CardList>
                <Text style ={{color:'red',textAlign:'center',marginBottom:5}}>{this.props.error}</Text> 
                </View>
                {this.renderButtons()}
            </ScrollView>
            )  
}}

const styles=StyleSheet.create({
    logo:{
        width:60,
        height:90,
        marginTop:40 
    },
    cont:{
        
        backgroundColor:'#fff',
        flex:1
    },
    icon:{
        width:27, height:23
    },
    text :{
        color: 'green'
    },
    text2:{
        textAlign: 'center',
        fontWeight:'500',
        fontSize:14,
        color:'#000',
        padding:20,
    }
})

const mapStateToProps =state =>{
    return {
        error : state.email.error,
        loading : state.email.loading,
        valid : state.email.valid,
        lang1: state.lang.language,

    }
}
//export componant for other apps
export default connect(mapStateToProps,{email_verify})(EmailVerify);

