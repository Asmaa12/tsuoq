import React ,{Component} from 'react';
import {Text,StyleSheet,View, ColorPropType, Image,ScrollView, TouchableOpacity, AsyncStorage} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';
import { connect } from 'react-redux';

import {Button,Card ,CardItem,Input,Spinner, Photo_upload,MyDatePicker1, CardList,Title} from './common';
var customData = require('./string/EditProfile.json');

var data = customData.English

class EditProfile extends Component {
    constructor(props){
        super(props);
        this.state ={

            name: '',
            username:'',
            email:'',
            phone_number:'',
            date_of_birth:'',
            gender:'',
            work:'',
            avatarSource: null,
            avatardata :null,
            photo :false,

            errorname: '',
            errorusername:'',
            erroremail:'',
            errorphone:'',
            errorpassword: '',
            errordate_of_birth:'',
            errorgender:'',
            errorwork:'',
            error:'',
            color:'#fff',
            loading: true,

            simpleDate: new Date(2000, 4, 5),
            minDate :new Date(1920,1,1),
            maxDate :new Date(),
            simpleText: 'Birth Date',
            
        };
    }

    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{
            data=customData. English
        
            }
  
    }
    
      selectPhotoTapped() {
        const options = {
          quality: 1.0,
          maxWidth: 200,
          maxHeight: 200,
          storageOptions: {
            skipBackup: true
          }
        };
    
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            let source = { uri: response.uri };
            console.log(response)
            console.log('User tapped custom button: ', source);
            this.setState({
              avatarSource: source,
              avatardata :  response.data,
              photo :true
            });
          }
        });
      }
      componentDidMount() {

   
                this.props.navigation.setParams({done:this.done.bind(this)}); 
                this.props.navigation.setParams({cancel:this.cancel.bind(this)});
                this.props.navigation.setParams({ reverse: this.props.reverse });


                AsyncStorage.getItem('username')
                .then(username=>{
                    axios.get('https://streetpal.org/api/user/'+ username +'/photo' )
                    .then(resp=>{
                        this.setState({avatarSource:{uri: 'https://streetpal.org/api/user/'+ username +'/photo' }})
                    })
                    })

                AsyncStorage.getItem('app_token')
                .then(token=>
                    axios.get('https://streetpal.org/api/me', { 'headers': { 'Authorization': 'Bearer ' + token } })
                    .then(resp=>{  this.setState({
                        name: resp.data.name,
                        username:resp.data.user,
                        email:resp.data.email,
                        phone_number:resp.data.phone,
                        date_of_birth:(resp.data.birth.y.toString())+'-'+(resp.data.birth.m.toString())+'-'+resp.data.birth.d.toString(),
                        gender:resp.data.gender,
                        work:resp.data.work ,
                        loading:false,
                        }) }  )
                    .catch(err=>console.log('error'))  
                )

               
              }
   
    componentWillUnmount(){
    
        AsyncStorage.getItem('username')
        .then(username=>{
            axios.get('https://streetpal.org/api/user/'+ username +'/photo' )
            .then(resp=>{
                this.setState({avatarSource:{uri: 'https://streetpal.org/api/user/'+ username +'/photo' }})
            })
            })
            

        AsyncStorage.getItem('app_token')
        .then(token=>
            axios.get('https://streetpal.org/api/me', { 'headers': { 'Authorization': 'Bearer ' + token } })
            .then(resp=>{  this.setState({
                name: resp.data.name,
                username:resp.data.user,
                email:resp.data.email,
                phone_number:resp.data.phone,
                date_of_birth:(resp.data.birth.y.toString())+'-'+(resp.data.birth.m.toString())+'-'+resp.data.birth.d.toString(),
                gender:resp.data.gender,
                work:resp.data.work ,
                loading: false}) }  )
            .catch(err=>console.log('error'))  
        )


        
    }      
         
    
            static navigationOptions = ({navigation})=>{
                
                
                 const {params ={}} =navigation.state;
                
                 const headerRight =(
                     <TouchableOpacity onPress={params.done}>
                      <Text style={{color:'#fff',fontSize:15,fontWeight:'600',padding:20}}> {data.x1} </Text>
                 </TouchableOpacity>
                   
                         );
                 const headerLeft =(
                         <TouchableOpacity onPress={params.cancel}>
                             <Text style={{color:'#fff',fontSize:15,fontWeight:'600',padding:10}}>{data.x2}</Text>
                        </TouchableOpacity>
                         );         
                                   
                 if(params.reverse){
                    return ({headerRight:headerLeft , headerLeft:headerRight});
                 }
                 else{
                    return ({headerRight:headerRight , headerLeft:headerLeft});
                 }
                     
                
                 }
       
                 cancel(){
                    this.props.navigation.navigate('Profile')
                }
        
      done=()=>{
           
            this.setState({
                errorname: '',
                erroremail:'',
                errorphone:'',
                errordate_of_birth:'',
                errorgender:'',
                errorwork:'',
                loading:true
                })
                var email=(this.state.email) 
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            //check name,username,email,work
            

            if(this.state.name.length==0){
                
               
                return(
                this.setState({errorname:data.x3}),
                this.setState({name:''}),
                this.setState({loading:false})
                )
            }
           
           
            else if( ! re.test(email)  ){
               
                
                return(
                this.setState({erroremail:data.x4}),
                this.setState({email:''}),
                this.setState({loading:false})
                )
            }
            else if((this.state.phone_number.length < 8)||(!(/^\d+$/.test(this.state.phone_number)))){
                
                
                return(
                this.setState({errorphone_number: data.x5 }),
                this.setState({phone_number:''}),
                this.setState({loading:false})
                )
            }
            else if(this.state.work.length<8){
                
               
                return(
                this.setState({errorwork: data.x6 }),
                this.setState({work:''}),
                this.setState({loading:false})
                )
            }

            //check birth date
           else if(this.state.date_of_birth==''){
            return(
                this.setState({errordate_of_birth: 'Birth Date' }),
                this.setState({loading:false})
            )
            }
            
            //check phone number
            else if((this.state.phone_number.length < 8)||(!(/^\d+$/.test(this.state.phone_number)))){
              
                
                return(
                this.setState({errorphone:data.x7 }),
                this.setState({phone_number:''}),
                this.setState({loading:false})
                )
            }

           
              
            else{
            
            const name =this.state.name;
            const gender =this.state.gender;
            const birth =this.state.date_of_birth;
            const email =this.state.email;
            const phone =this.state.phone_number;
            const work =this.state.work;
            const avatar = this.state.avatardata 
            //console.log('fff')
            return(
            AsyncStorage.getItem('app_token')
            .then(token=>
                axios.post('https://streetpal.org/api/me',{name,gender,
                birth:{d: Number(birth.split("-")[2]),m:Number(birth.split("-")[1]),y:Number(birth.split("-")[0])},
                email,phone,work},{ 'headers': { 'Authorization': 'Bearer ' + token}})
                    .then(resp=>{
                        console.log(resp)
                        if(this.state.photo){
                            axios.post('https://streetpal.org/api/me/photo',{base64:avatar},{ 'headers': { 'Authorization': 'Bearer ' + token ,'Content-Type':'application/json'} })
                            .then(resp=>{
                               
                                console.log('uploaded'),
                                this.setState({loading:false}),this.props.navigation.navigate('Profile')})
                            .catch(err=>{
                                
                               
                                console.log(err.response.data),
                                this.setState({loading:false,error:data.x8})})   }
                        else{
                            this.setState({loading:false}),
                            this.props.navigation.navigate('Profile')
                        }    
                        
                        }
                    )
                    .catch(err=>{
                        
                        
                        console.log(err.response.data),
                        this.setState({loading:false,error:data.x8})}
                    )
            )
        )
            
        }
            

      }
      showPicker = async (stateKey, options) => {
        if(Platform.OS === 'android'){

        try {
          var newState = {};
          const {action, year, month, day} = await DatePickerAndroid.open(options);
          if (action === DatePickerAndroid.dismissedAction) {
            newState[stateKey + 'Text'] = 'dismissed';
          } else {
            var date = new Date();
            console.log(date.toLocaleDateString())
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var yy = date.getFullYear();
            this.setState({date_of_birth:yy + '-' + mm + '-' + dd})
            newState[stateKey + 'Text'] = yy + '-' + mm + '-' + dd;
            newState[stateKey + 'Date'] = date;
          }
          this.setState(newState);
        } catch ({code, message}) {
          console.warn(`Error in example '${stateKey}': `, message);
        }
    }


      };
 

    render=()=>{
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{ AsyncStorage.getItem('language')
        .then(lang=>{
            if(lang=='Arabic'){
            data=customData. Arabic
        }
            else{
                data=customData. English
            }
    })
}


        if(this.state.loading){
            return ( <Spinner/>)
        }
       
        return(
            <ScrollView >
            
                <View style={{margin:10}}>
                <Photo_upload avatarSource={this.state.avatarSource} onPress={this.selectPhotoTapped.bind(this)}/>
                </View>

                <CardList>
                
                    <Input    
                            placeholder ={data.x9}
                            securetext = {false}
                            //autoFocus ={true}
                            error={this.state.errorname}
                            editable ={!(this.props.loading)}
                            value={this.state.name}
                            onChangeText ={(name)=> {this.setState({name})}
                                }/>

                         
                    
                    <Input  
                            placeholder ={data.x10}
                            securetext = {false}
                            editable ={false}
                            value={this.state.username}
                            
                            />  

                     <Input   
                            placeholder ={data.x11} 
                            securetext = {false}
                            keyboardType ='email-address'
                            error={this.state.erroremail}
                            editable ={!(this.props.loading)}
                            value={this.state.email}
                            onChangeText ={(email)=> this.setState({email})}/> 

                     <Input   
                            placeholder ={data.x12} 
                            securetext = {false}
                            keyboardType = 'phone-pad'
                            error={this.state.errorphone}
                            editable ={!(this.props.loading)}
                            value={this.state.phone_number}
                            onChangeText ={(phone_number)=> this.setState({phone_number})}/> 
  
                </CardList> 

                
                
                <Title>{data.x13}</Title>
                <CardList>

                    <View style ={{flexDirection :'row',}}>
                    <MyDatePicker1
                        simpleText={this.state.simpleText}
                           onPress={this.showPicker.bind(this, 'simple', 
                           {date: this.state.simpleDate ,maxDate:this.state.maxDate , minDate:this.state.minDate})} 
                        />
                        <Input  
                            placeholder ='gender' 
                            securetext = {false}
                            editable ={false}
                            value={this.state.gender}
                          /> 

                      
                    </View>

                    <Input  
                            placeholder ={data.x14} 
                            securetext = {false}
                            editable ={!(this.props.loading)}
                            error={this.state.errorwork}
                            value={this.state.work}
                            onChangeText ={(work)=> this.setState({work})}/> 


                    
                    <Text style ={{color:'red',textAlign:'center',marginBottom:5}}>{this.state.error}</Text>            
                    
                    
            </CardList>
             
            </ScrollView>
            )}
}

const styles=StyleSheet.create({

    
    bu:{
        marginBottom:20,
        flex :1,
    },
    
    
})
const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        reverse:state.lang.reverse
    }
}

export default connect(mapStateToProps, { })(EditProfile);

//export componant for other apps


