import React ,{Component} from 'react';
import {Text,StyleSheet,View, ColorPropType,Platform,
    Image,ScrollView, TouchableOpacity, AsyncStorage,
    Picker,DatePickerAndroid,} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';

import {Button,Card ,CardItem,Input,Spinner, Photo_upload, CardList,Title,MyDatePicker1} from './common';
import {signup} from './actions';
var customData = require('./string/SignupForm.json');

var data = customData.English

class SignupForm extends Component {
    constructor(props){
        super(props);
        this.state ={
           
            name: '',
            username:'',
            email:'',
            phone_number:'',
            password: '',
            date_of_birth:'',
            gender:'sex',
            work:'',
            avatarSource: null,
            avatardata :null,

            errorname: '',
            errorusername:'',
            erroremail:'',
            errorphone:'',
            errorpassword: '',
            errordate_of_birth:'',
            errorgender:'',
            errorwork:'',

            color:'#fff',

            
    simpleDate: new Date(2000, 4, 5),
    minDate :new Date(1920,1,1),
    maxDate :new Date(),
    simpleText: 'Birth Date',
          
    
        };
    }
    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
            
        }
        else{
            data=customData.English
            
            }
    }
  
    static navigationOptions = ()=>{
        
         return ({title:data.x17 });
     }

    componentWillReceiveProps(nextProps){
        if(nextProps.token){
            this.props.navigation.navigate('EmailVerify');
        }
    }
    
      selectPhotoTapped() {
        const options = {
          quality: 1.0,
          maxWidth: 200,
          maxHeight: 200,
          storageOptions: {
            skipBackup: true
          }
        };
    
        ImagePicker.showImagePicker(options, (response) => {
          //console.log('Response = ', response);
    
          if (response.didCancel) {
            //console.log('User cancelled photo picker');
          }
          else if (response.error) {
            //console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            //console.log('User tapped custom button: ', response.customButton);
          }
          else {
            let source = { uri: response.uri };
          
            this.setState({
              avatarSource: source,
              avatardata :  response.data
            });
          }
        });
      }


      signuppress=()=>{
           
            this.setState({
                errorname: '',
                errorusername:'',
                erroremail:'',
                errorphone:'',
                errorpassword: '',
                errordate_of_birth:'',
                errorgender:'',
                errorwork:'',
                color:'#fff'})

            //check name,username,email,work
           

            if(this.state.name.length==0){
            
                
                this.setState({errorname:data.x1}),
                this.setState({name:''})
            }

            if(this.state.username.length<6){
               
              
                this.setState({errorusername:data.x2}),
                this.setState({username:''})
            }

            var x1=(this.state.email) 
            
           var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if( ! re.test(x1)  ){
                
               
                this.setState({erroremail:data.x3}),
                this.setState({email:''})
                
            }

          

            if(this.state.work.length<8){
                
                
                this.setState({errorwork: data.x4 }),
                this.setState({work:''})
            }

            //check birth date
            if(this.state.date_of_birth==''){
                this.setState({errordate_of_birth: 'Birth Date' })
            }
            
            //check phone number
            if((this.state.phone_number.length < 8)||(!(/^\d+$/.test(this.state.phone_number)))){
                
                this.setState({errorphone: data.x5 }),
                this.setState({phone_number:''})
            }

            //check gender
            if(this.state.gender=='sex'){
                this.setState({color: 'red' })
            }

            //check password
            if(!( (this.state.password.toUpperCase() != this.state.password)&&
            (this.state.password.toLowerCase()!=this.state.password)&&
            (/\d/.test(this.state.password))&&(this.state.password.length>8) )){
                
                
                this.setState({errorpassword:data.x6}),
                this.setState({password:''})
            }

           
               
            if((this.state.username.length>5)&&
            re.test(x1) && 
            (this.state.work.length>7)&& 
            (this.state.name.length>0)&&                                   //valid name,username.email,work
            (this.state.password.toUpperCase() != this.state.password)&&  //valid password
            (this.state.password.toLowerCase()!=this.state.password)&&
            (/\d/.test(this.state.password))&&  
            (this.state.password.length>=8)&&
            (this.state.gender!='sex')&&                                  //valid gender
            (this.state.date_of_birth!='')&&                              //valid phone number
            (this.state.phone_number.length>=8)&&
            (/^\d+$/.test(this.state.phone_number))){
            
            const user=this.state.username;
            const pass=this.state.password;
            const name =this.state.name;
            const gender =this.state.gender;
            const birth =this.state.date_of_birth;
            const email =this.state.email;
            const phone =this.state.phone_number;
            const work =this.state.work;
            const avatar = this.state.avatardata 
            
            this.props.signup({user,pass,name,gender,birth,email,phone,work,avatar });
            
        }
            

      }

      showPicker = async (stateKey, options) => {
        if(Platform.OS === 'android'){

        try {
          var newState = {};
          const {action, year, month, day} = await DatePickerAndroid.open(options);
          if (action === DatePickerAndroid.dismissedAction) {
            newState[stateKey + 'Text'] = 'dismissed';
          } else {
            var date = new Date();
            console.log(date.toLocaleDateString())
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var yy = date.getFullYear();
            this.setState({date_of_birth:yy + '-' + mm + '-' + dd})
            newState[stateKey + 'Text'] = yy + '-' + mm + '-' + dd;
            newState[stateKey + 'Date'] = date;
          }
          this.setState(newState);
        } catch ({code, message}) {
          console.warn(`Error in example '${stateKey}': `, message);
        }
    }


      };

    _renderButton(){
        if(this.props.loading){
            return <Spinner/>
        }
        else{
            
            return (
                <Button height ={52} color={'#FDAC60'} onpress ={this.signuppress.bind(this)} >{data.x17}</Button>
            )    
    }
    };

    render=()=>{
       
    
        return(
            <ScrollView >
            
                <View style={{  margin:20,justifyContent : 'center',alignItems : 'center',}}>
                    <Photo_upload avatarSource={this.state.avatarSource} onPress={this.selectPhotoTapped.bind(this)}/>
                    <Text style={styles.contt}>{data.x18}</Text>
                </View>
                <CardList>
                
                    <Input    
                            placeholder ={data.x7}
                            securetext = {false}
                            //autoFocus ={true}
                            error={this.state.errorname}
                            editable ={!(this.props.loading)}
                            value={this.state.name}
                            onChangeText ={(name)=> {this.setState({name})}
                                }/>

                         
                    
                    <Input  
                            placeholder ={data.x8} 
                            securetext = {false}
                            error={this.state.errorusername}
                            editable ={!(this.props.loading)}
                            value={this.state.username}
                            onChangeText ={(username)=> this.setState({username})}/>  

                     <Input   
                            placeholder ={data.x9}
                            securetext = {false}
                            keyboardType ='email-address'
                            error={this.state.erroremail}
                            editable ={!(this.props.loading)}
                            value={this.state.email}
                            onChangeText ={(email)=> this.setState({email})}/> 

                     <Input   
                            placeholder ={data.x10} 
                            securetext = {false}
                            keyboardType = 'phone-pad'
                            error={this.state.errorphone}
                            editable ={!(this.props.loading)}
                            value={this.state.phone_number}
                            
                            onChangeText ={(phone_number)=> this.setState({phone_number})}/> 

                     <Input  
                            placeholder ={data.x11} 
                            securetext = {true}
                            error={this.state.errorpassword}
                            editable ={!(this.props.loading)}
                            value={this.state.password}
                            
                            onChangeText ={(password)=> this.setState({password})}/>   
                </CardList> 

                
                
                <Title>{data.x12}</Title>
                <CardList>

                    
                        <MyDatePicker1
                        simpleText={this.state.simpleText}
                           onPress={this.showPicker.bind(this, 'simple', 
                           {date: this.state.simpleDate ,maxDate:this.state.maxDate , minDate:this.state.minDate})} 
                        />

                    <View style ={{borderWidth:0,borderBottomWidth : 1,borderBottomColor:this.state.color,}}>
                        <Picker
                            selectedValue={this.state.gender}
                            style = {{width:150}}
                            onValueChange={(gender) => {this.setState({gender: gender})}}>
                            <Picker.Item label={data.x13} value="sex" />
                            <Picker.Item label={data.x14} value="female" />
                            <Picker.Item label={data.x15} value="male" />
                        </Picker>
                       

                        
                    </View>

                    <Input  
                            placeholder ={data.x16}
                            securetext = {false}
                            editable ={!(this.props.loading)}
                            error={this.state.errorwork}
                            value={this.state.work}
                            
                            onChangeText ={(work)=> this.setState({work})}/> 
                            
                        

                    
                    <Text style ={{color:'red',textAlign:'center',marginBottom:5}}>{this.props.error}</Text>            
                    <View style ={styles.bu}> 
                        {this. _renderButton()}
                    </View>
                    
            </CardList>
             
            </ScrollView>
            )
    }
}

const styles=StyleSheet.create({

    contt:{
        
            padding: 10,
            fontSize:15,
            fontWeight:'600',
            justifyContent : 'center',
            alignItems : 'center',
            color:'#000'
        },
    bu:{
        marginBottom:20,
        flex :1,
    },
    
    
})


const mapStateToProps =state =>{
    return {
        error : state.sign.error,
        loading : state.sign.loading,
        token :state.sign.token,
        lang1: state.lang.language
    }
}
//export componant for other apps
export default connect(mapStateToProps,{signup})(SignupForm);

