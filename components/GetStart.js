import React ,{Component} from 'react';
import {Text,StyleSheet,View, ColorPropType, Image,Animated, TouchableOpacity, AsyncStorage,ScrollView} from 'react-native';
import axios from 'axios';
import { Button } from './common';
import { connect } from 'react-redux';
var customData = require('./string/GetStart.json');

var data = customData.English

class GetStart extends Component{
    
   
      constructor(){
        super();
        this.state ={
            user:'',
            username:'',
            photo : ''
        };
    }

    
    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{
            data=customData. English
        
            }
    }

     start(){
        this.props.navigation.navigate('Profile');
     }
    
    render(){
         
        return(
            <ScrollView style={styles.cont}>
                
                <View style={{flex:1, justifyContent : 'center',alignItems : 'center',}}>
                <Image source={require('./photo/correct.png')} style={styles.image}/>
                <Text style={styles.contt}>{data.x1}</Text>
                </ View>
                <Button height ={52} color={'#FDAC60'} onpress ={this.start.bind(this)}>{data.x2}</Button>
                
            </ScrollView>
            )}
}

const styles=StyleSheet.create({
    contt:{
    
        padding: 10,
        margin : 30,
       fontSize:25,
       fontWeight:'300',
       color:'#000'
    },
   
    image :{
        height:100,
        width :100,
        marginTop:80, 
    
    },
    cont:{
       
        backgroundColor:'#fff', 
    
    },
    
})

const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        
    }
}

export default connect(mapStateToProps,{ })(GetStart);
