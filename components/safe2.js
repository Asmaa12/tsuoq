
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,Dimensions,Image, TouchableOpacity,Alert,Linking,ActivityIndicator,AsyncStorage,
} from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import axios from 'axios';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';

import { Loading, Button, Spinner} from './common/index';
import { connect } from 'react-redux';
import Sound from 'react-native-sound';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
var customData = require('./string/safe.json');

var data = customData.English

const { width, height}=Dimensions.get('window')
const ASPECT_RATIO = (width/height)
const LATTITUDE_DELTA = 0.01822
const LONGTITUTE_DELTA = LATTITUDE_DELTA *(width/height)

class Safe2 extends Component<{}> {
 // static navigationOptions = { };

  constructor(){
    super();
    this.state ={
        record:false,
        currentTime: 0.0,
        initialPosition: {
          latitude: 0,
          longitude: 0,
          latitudeDelta: 0,
          longitudeDelta: 0,
        },

        markerPosition :{
          latitude: 0,
          longitude: 0,
        },

        latitude_police:0,
        longitude_police:0,

        longitude_hospital:0,
        latitude_hospital:0,
      
        d1:null,
        d2:null,

        click:false,
        progressVisible:true,
        
    
    }
  }

  componentWillMount(){
    if(this.props.lang1=='Arabic'){
        data=customData.Arabic
    }
    else{
        data=customData. English
    
        }
}
static navigationOptions = ({ navigation }) => {
    
            const { params = {} } = navigation.state;

    
            if (params.reverse) {
                return ({ title: data.x6 });
            }
            else {
                return ({  title: data.x6 });
            }
        }

  watchID : ?number =null
  componentWillUnmount () {
    navigator.geolocation.clearWatch(this.watchID)
  }
  componentDidMount(){
////////////////////////////////
    AudioRecorder.onProgress = (data) => {
        this.setState({currentTime: Math.floor(data.currentTime)});
      };

      AudioRecorder.onFinished = (data) => {
        // Android callback comes in the form of a promise instead.
        if (Platform.OS === 'ios') {
          this._finishRecording(data.status === "OK", data.audioFileURL);
        }
      };
      //////////////////////
    navigator.geolocation.getCurrentPosition((position) =>{
      var lat = parseFloat(position.coords.latitude)
      var long = parseFloat(position.coords.longitude)

      var initialRegion ={
        latitude : lat,
        longitude : long,
        latitudeDelta : LATTITUDE_DELTA,
        longitudeDelta : LONGTITUTE_DELTA
      }
      this.setState({initialPosition: initialRegion})
      this.setState({markerPosition: initialRegion})
    },

    {enableHighAccuracy: true, timeout:20000 , maximumAge:1000})

    this.watchID = navigator.geolocation.watchPosition((position) => {
      // Create the object to update this.state.mapRegion through the onRegionChange function
    var lat =parseFloat(position.coords.latitude)
    var long = parseFloat (position.coords.longitude)

    var lastRegion ={
      latitude : lat ,
      longitude : long,
      longitudeDelta : LONGTITUTE_DELTA,
      latitudeDelta : LATTITUDE_DELTA,
    }

    this.setState({initialPosition: lastRegion})
    this.setState({markerPosition: lastRegion})
 
      //console.log('police')
      this.GetNearestPolice()
    

    }
    
    );

  }

  

 

  GetNearestPolice=()=>{
    
   
    this.setState({progressVisible:true})
    axios.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+ this.state.initialPosition.latitude+','+this.state.initialPosition.longitude+'&rankby=distance&types=police&key=AIzaSyBHTlp76RyPgdtiHr7Aq9ULX69EfyomkqI')
     .then(result=>{
    
       console.log(result.data.results[0].geometry.location.lat),
       console.log(result.data.results[0].geometry.location.lng),
       
      this.setState({latitude_police:result.data.results[0].geometry.location.lat , longitude_police:result.data.results[0].geometry.location.lng});
      this.setState({progressVisible:false})
    
    })
     .catch(err=>{
      this.setState({progressVisible:false}),
       console.log(err)})
   
    }

    load=()=>{
      
      if(this.state.progressVisible){
        return (
          <View style={styles.loading}>
              <ActivityIndicator size={ 'large'} />
          </View>
       )
      }

    }

    call_trusted(){
        if(this.props.token){
            axios.get('https://streetpal.org/api/me/trusted', { 'headers': { 'Authorization':'Bearer ' + this.props.token} })
            .then(resp=>{
                RNImmediatePhoneCall.immediatePhoneCall(resp.data.phone); })  
            .catch(err=>{console.log(err.data)})
        }
        else{
        AsyncStorage.getItem('app_token')
        .then(token =>{
        axios.get('https://streetpal.org/api/me/trusted', { 'headers': { 'Authorization':'Bearer ' + token} })
        .then(resp=>{
            RNImmediatePhoneCall.immediatePhoneCall(resp.data.phone); })  
        .catch(err=>{console.log(err.data)}
            
        )  
       })
    }
    }

    async record(){
        if(this.state.record){
        
             this.setState({record:false});
             try {
                const filePath = await AudioRecorder.stopRecording();
        
                if (Platform.OS === 'android') {
                    console.log(`Finished recording of duration ${this.state.currentTime} seconds at path: ${filePath}`);
                }
                return filePath;
              } catch (error) {
                console.error(error);
              }
            
             
        }
        else{
            //AudioUtils.DocumentDirectoryPath + '/ttte.aac';
            let d = new Date();
            let n = d.getTime();
           // let path = this.state.audioPath+
            audioPath=AudioUtils.MusicDirectoryPath +"/"+n+".aac";
            AudioRecorder.prepareRecordingAtPath(audioPath, {
              SampleRate: 22050,
              Channels: 1,
              AudioQuality: "Low",
              AudioEncoding: "aac",
              AudioEncodingBitRate: 32000
            });
      
            this.setState({record:true});
            try {
              const filePath = await AudioRecorder.startRecording();
            } catch (error) {
              console.error(error);
            }

        }
    }

    go_police(){
      Linking.openURL('https://www.google.es/maps/dir/'+"'"+this.state.initialPosition.latitude+','+this.state.initialPosition.longitude+ "'/'"+this.state.latitude_police+','+this.state.longitude_police+"'" ) 
    }
  
  render() {
    
    
    
    return (
      <View style={{flex:1,justifyContent:'center'}}>
            
        <View style={styles.container}>
            <MapView
            style={styles.map}
            region={
                this.state.initialPosition
            }
            showsUserLocation={true}
            followUserLocation={true}
            minZoomLevel={1}
            >
            

            <MapView.Marker
                //pinColor ='#174442'
                //title ='Police Station'
                image={require('./photo/marker_police_30.png')}
                coordinate={{latitude:this.state.latitude_police, longitude:this.state.longitude_police}}>
                
            </MapView.Marker> 


    
            <MapView.Marker
                //pinColor ='#FDAC60'
                //title ='You'
            image={require('./photo/guide1.png')}
                coordinate={this.state.initialPosition}>
                
            </MapView.Marker> 

    
            <MapViewDirections
            origin={this.state.initialPosition}
            destination= {{latitude: this.state.latitude_police, longitude: this.state.longitude_police}}
            apikey={'AIzaSyBHTlp76RyPgdtiHr7Aq9ULX69EfyomkqI'}
            strokeWidth={5}
            strokeColor="#FDAC60"
            /> 
            
                
            </MapView>
        

            {this.load()}
        </View>

        <View style={{position:'absolute',top:Dimensions.get('window').height/2 ,flex:1,justifyContent:'center',alignItems:'center'}}>
        
            <View style={{flexDirection:'row'}}>
            {this.state.record ?
             <TouchableOpacity style={{width:20,height:20,borderRadius:10,backgroundColor:'red',margin:10,marginRight:(Dimensions.get('window').width-40)}} 
             onPress={this.record.bind(this)}/>
             :
             <TouchableOpacity style={{width:20,height:20,borderRadius:10,backgroundColor:'#FDAC60',margin:10,marginRight:(Dimensions.get('window').width-40)}} 
             onPress={this.record.bind(this)}/>
            }
                <View style={{position:'absolute',top:10,right:0,flexDirection:'row'}}>
                <Text style={{fontSize:15,fontWeight:'400',color:'#000',}} >{this.state.currentTime} s    </Text>
                <Text style={{fontSize:15,fontWeight:'400',color:'#000',}}>{data.x1}</Text>
                </View>
            </View>

            <View style={{width:Dimensions.get('window').width,flex:1,}}>
                <Button height ={50} color={'#293030'} onpress={this.go_police.bind(this)}>{data.x2}</Button>
            </View>

            <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('Chat');
                        }}>
                        <Text style={{textDecorationLine: 'underline',padding:5}}>{data.x3}</Text>
            </TouchableOpacity>
            
      
        </View>

        <View style={{flexDirection:'row',position:'absolute',bottom:0}}>
            <TouchableOpacity onPress={ this.call_trusted.bind(this)} 
            style={{backgroundColor:"#FDAC60",height:50,width:(Dimensions.get('window').width/2)-2 ,margin:1,
            justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#fff',fontWeight:'bold'}}>{data.x4}</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile')} 
             style={{backgroundColor:"#FDAC60",height:50,width:(Dimensions.get('window').width/2)-2 ,margin:1,
             justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#fff',fontWeight:'bold'}}>{data.x5}</Text>
            </TouchableOpacity>
        </View>
      
    </View>    
    );
  



}
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',borderBottomWidth:2,borderColor:"#FDAC60",
    alignItems: 'center',height:Dimensions.get('window').height/2
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex:-1
  },
  icon:{
    width:20, height:23
},  

loading: {
    zIndex:3,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    //opacity: 0.5,
    backgroundColor:'rgba(0,0,0,.5)',
    justifyContent: 'center',
    alignItems: 'center',
}

});

const mapStateToProps = state => {
  return {
      lang1: state.lang.language,
      token : state.auth.token,
     
  }
}

export default connect(mapStateToProps,{ })(Safe2);
