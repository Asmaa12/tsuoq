// @flow

import React, { Component } from 'react';
import { Button, FlatList, SafeAreaView, StyleSheet, Text,Dimensions, TouchableHighlight, View, addons,Image ,AsyncStorage,ScrollView,} from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios'
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import strs from './string/Chat';
import { ChatItem ,Choose } from './common';
var i=0;
var x;
const scenarios = strs => ({
    
    USER_FEELS_FOLLOWED: [
        { style: 'RECEIVED', value: strs.user_guide_case_1_question1 },
        { style: 'OPTION', value: strs.user_guide_positive_button, next: 'USER_IS_FOLLOWED_SURROUNDINGS_ARE_SAFE' },
        { style: 'OPTION', value: strs.user_guide_negative_button, next: 'USER_IS_FOLLOWED_SURROUNDINGS_ARE_NOT_SAFE' },
        { style: 'OPTION', value: strs.user_guide_agnostic_button, next: 'DOESNT_KNOW_IF_SORROUNDINGS_ARE_SAFE' },
    ],
    USER_IS_FOLLOWED_SURROUNDINGS_ARE_SAFE: [
        { style: 'RECEIVED', value: strs.user_guide_case_1_part1 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_part2 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_part3 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_part4 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    USER_IS_FOLLOWED_SURROUNDINGS_ARE_NOT_SAFE: [
        { style: 'RECEIVED', value: strs.user_guide_user_sorroundings_not_safe_part1 },
        { style: 'RECEIVED', value: strs.user_guide_user_sorroundings_not_safe_part3 },
        { style: 'RECEIVED', value: strs.user_guide_user_sorroundings_not_safe_part4, next: 'CALL_TRUSTED_CONTACT' },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    DOESNT_KNOW_IF_SORROUNDINGS_ARE_SAFE: [
        { style: 'RECEIVED', value: strs.user_guide_user_doesnt_know_sorroundings_safe_part1 },
        { style: 'RECEIVED', value: strs.user_guide_user_doesnt_know_sorroundings_safe_part2 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question1 },
        { style: 'OPTION', value: strs.user_guide_positive_button, next: 'USER_IS_FOLLOWED_SURROUNDINGS_ARE_SAFE' },
        { style: 'OPTION', value: strs.user_guide_negative_button, next: 'USER_IS_FOLLOWED_SURROUNDINGS_ARE_NOT_SAFE' },
        { style: 'OPTION', value: strs.user_guide_agnostic_button, next: 'DOESNT_KNOW_IF_SORROUNDINGS_ARE_SAFE' },
    ],
    USER_FEELS_IN_DANGER: [
        { style: 'RECEIVED', value: strs.user_in_danger_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_part2 },
        { style: 'RECEIVED', value: strs.user_in_danger_part3 },
        { style: 'RECEIVED', value: strs.user_in_danger_part4 },
        { style: 'RECEIVED', value: strs.user_in_danger_part5 },
        { style: 'OPTION', value: strs.user_in_danger_first_question_answer_1, next: 'PEOPLE_NOT_HELPING' },
        { style: 'OPTION', value: strs.user_in_danger_first_question_answer_2, next: 'PEOPLE_HELPING_NOT_GOING_TO_POLICE' },
        { style: 'OPTION', value: strs.user_in_danger_first_question_answer_3, next: 'GOING_TO_POLICE_STATION' },
    ],
    PEOPLE_NOT_HELPING: [
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part2, next: 'HOSPITAL_INSTRUCTIONS' },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part3, next: 'POLICE_INSTRUCTIONS' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    PEOPLE_HELPING_NOT_GOING_TO_POLICE: [
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_help_not_police_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_help_not_police_part2, next: 'PEOPLE_NOW_HELPING' },
        { style: 'RECEIVED', value: strs.user_in_danger_help_not_police_part3 },
        { style: 'RECEIVED', value: strs.user_in_danger_help_not_police_part4, next: 'HOSPITAL_INSTRUCTIONS' },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part3, next: 'POLICE_INSTRUCTIONS' },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    GOING_TO_POLICE_STATION: [
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_going_police_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_going_police_part2 },
        { style: 'RECEIVED', value: strs.police_station_package_1, next: 'POLICE_STATION' },
        { style: 'RECEIVED', value: strs.police_station_package_2 },
        { style: 'RECEIVED', value: strs.police_station_package_3 },
        { style: 'RECEIVED', value: strs.police_station_package_4, next: 'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.police_station_package_5 },
        { style: 'RECEIVED', value: strs.police_station_package_6 },
        { style: 'RECEIVED', value: strs.police_station_package_7 },
        { style: 'RECEIVED', value: strs.police_station_package_8, next: 'CALL_NADEEM' },
        { style: 'RECEIVED', value: strs.police_station_package_9, next: 'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.police_station_package_10 },
        { style: 'RECEIVED', value: strs.police_station_package_11, next: 'HOSPITAL' },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    USER_IS_SAFE: [
        { style: 'RECEIVED', value: strs.user_guide_user_safe },
        { style: 'OPTION', value: strs.user_guide_terminate_chat, next: 'TERMINATE_CHAT' },
    ],
    SEND_STRESS_SIGNAL: [
        { style: 'RECEIVED', value: strs.user_guide_user_not_safe_part1 },
        { style: 'RECEIVED', value: strs.user_guide_user_not_safe_part2 },
        { style: 'OPTION', value: strs.user_guide_case_1, next: 'USER_FEELS_FOLLOWED' },
        { style: 'OPTION', value: strs.user_guide_case_2, next: 'USER_FEELS_IN_DANGER' },
        { style: 'OPTION', value: strs.user_guide_neutral_button, next: 'USER_HAS_BEEN_HARASSED' },
    ],
    IN_WAY_TO_HOSPITAL: [
        { style: 'RECEIVED', value: strs.hospital_package_1, next: 'HOSPITAL' },
        { style: 'RECEIVED', value: strs.hospital_package_2 },
        { style: 'RECEIVED', value: strs.hospital_package_3 },
        { style: 'RECEIVED', value: strs.hospital_package_4, next: 'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.hospital_package_5, next: 'POLICE_STATION' },
        { style: 'RECEIVED', value: strs.hospital_package_6 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    IN_WAY_TO_POLICE: [
        { style: 'RECEIVED', value: strs.police_station_package_1, next: 'POLICE_STATION' },
        { style: 'RECEIVED', value: strs.police_station_package_2 },
        { style: 'RECEIVED', value: strs.police_station_package_3 },
        { style: 'RECEIVED', value: strs.police_station_package_4, next: 'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.police_station_package_5_anotherCase },
        { style: 'RECEIVED', value: strs.police_station_package_6_anotherCase },
        { style: 'RECEIVED', value: strs.police_station_package_6 },
        { style: 'RECEIVED', value: strs.police_station_package_7 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    USER_HARASSED_WHAT_TO_DO: [
        { style: 'RECEIVED', value: strs.user_in_danger_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_part2 },
        { style: 'RECEIVED', value: strs.user_been_harassed_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part2, next: 'HOSPITAL_INSTRUCTIONS' },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part3, next: 'POLICE_INSTRUCTIONS' },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    USER_IN_POLICE_STATION: [
        { style: 'RECEIVED', value: strs.user_in_danger_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_part2 },
        { style: 'RECEIVED', value: strs.police_station_package_2 },
        { style: 'RECEIVED', value: strs.police_station_package_3 },
        { style: 'RECEIVED', value: strs.police_station_package_4, next: 'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.police_station_package_5 },
        { style: 'RECEIVED', value: strs.police_station_package_6 },
        { style: 'RECEIVED', value: strs.police_station_package_7 },
        { style: 'RECEIVED', value: strs.police_station_package_11, next: 'HOSPITAL' },
        { style: 'RECEIVED', value: strs.police_station_package_8, next: 'CALL_NADEEM' },
        { style: 'RECEIVED', value: strs.police_station_package_9, next: 'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.police_station_package_10 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    USER_IS_INJURED: [
        { style: 'RECEIVED', value: strs.user_in_danger_part1 },
        { style: 'RECEIVED', value: strs.user_in_danger_part2 },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part2_Replacement, next: 'HOSPITAL_INSTRUCTIONS' },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part3, next: 'POLICE_INSTRUCTIONS' },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    USER_NEEDS_TO_TALK: [
        { style: 'RECEIVED', value: strs.user_needs_to_talk_part1 },
        { style: 'RECEIVED', value: strs.police_station_package_8, next: 'CALL_NADEEM, false' },
        { style: 'RECEIVED', value: strs.police_station_package_9, next: 'CALL_NAZRA, false' },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part2_Replacement, next: 'HOSPITAL_INSTRUCTIONS, false' },
        { style: 'RECEIVED', value: strs.user_in_danger_no_help_part3, next: 'POLICE_INSTRUCTIONS, false' },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    USER_FILED_REPORT: [
        { style: 'RECEIVED', value: strs.police_station_package_4, next: 'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.police_station_package_5 },
        { style: 'RECEIVED', value: strs.police_station_package_6 },
        { style: 'RECEIVED', value: strs.police_station_package_7 },
        { style: 'RECEIVED', value: strs.police_station_package_11, next: 'HOSPITAL' },
        { style: 'RECEIVED', value: strs.police_station_package_8, next: 'CALL_NADEEM' },
        { style: 'RECEIVED', value: strs.police_station_package_9, next: 'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.police_station_package_10 },
        { style: 'RECEIVED', value: strs.police_station_package_12 },
        { style: 'RECEIVED', value: strs.police_station_package_13 },
        { style: 'RECEIVED', value: strs.police_station_package_14 },
        { style: 'OPTION', value: strs.user_guide_terminate_chat, next: 'TERMINATE_CHAT' },
    ],
    HOSPITAL_INSTRUCTIONS :[
        { style: 'RECEIVED', value: strs.x1 },
        { style: 'RECEIVED', value: strs.x2 },
        { style: 'RECEIVED', value: strs.x3 },
        { style: 'RECEIVED', value: strs.x4 },
        { style: 'RECEIVED', value: strs.x5, next: 'POLICE_INSTRUCTIONS' },
        { style: 'RECEIVED', value: strs.x6 },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
        
    ],
    POLICE_INSTRUCTIONS :[
        { style: 'RECEIVED', value: strs.y1,next :'POLICE_STATION' },
        { style: 'RECEIVED', value: strs.y2 },
        { style: 'RECEIVED', value: strs.y3 },
        { style: 'RECEIVED', value: strs.y4,next: 'CALL_NAZRA'},
        { style: 'RECEIVED', value: strs.y5 },
        { style: 'RECEIVED', value: strs.y6 },
        { style: 'RECEIVED', value: strs.y7 },
        { style: 'RECEIVED', value: strs.y8 },
        { style: 'RECEIVED', value: strs.y9, next: 'CALL_NADEEM' },
        { style: 'RECEIVED', value: strs.y10 ,next:'CALL_NAZRA' },
        { style: 'RECEIVED', value: strs.user_guide_case_1_question2 },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_positive_button, next: 'USER_IS_SAFE' },
        { style: 'OPTION', value: strs.user_guide_case1_answer2_negative_button, next: 'SEND_STRESS_SIGNAL' },
    ],
    TERMINATE_CHAT: [],
    USER_HAS_BEEN_HARASSED: [],
});

const BigButton = ({ value, ...props }) => (
    <TouchableHighlight  style={{margin:30,marginBottom:0,borderRadius:5}} {...props}>
    <View style={[styles.bigButtonContainer]}>
        <Image source={require('./photo/q2.png')}  style={{ width:50, height:50, }}/>
        <View style={{ width:Dimensions.get('window').width-160  }}>
        <Text style={styles.bigButtonText}>{value}</Text>
        </View>
    </View>    
    </TouchableHighlight>
);

const SmallButton = ({ value, ...props }) => (
    <TouchableHighlight style={{margin:10,marginBottom:0,borderRadius:5}} {...props}>
    <View style={[styles.smallButtonContainer]}>
        <Image source={require('./photo/connect.png')}  style={{ width:30, height:30, }}/>
        <View style={{ width:Dimensions.get('window').width-120  }}>
        <Text style={styles.smallButtonText}>{value}</Text>
        </View>
    </View>    
    </TouchableHighlight>
);

const ChatReceived = ({ value, next, cb,index, ...props }) => (
  
        <ChatItem   message={value}  id={1}   onpress={() => cb(next)} />
   
);

const ChatSent = ({ value, next, cb, ...props }) => (
    <ChatItem   message={value} id={0}   />
);

const ChatOption = ({ value, next, cb, ...props }) => (
    console.log(value) ||<Choose onpress={() => cb(next)}>{value} </Choose> 
    
);


class Chat extends Component /*:: <Props, State> */ {
    state = {
        scenario: '',
        chatList: [],

        Position :{
            latitude: 0,
            longitude: 0,
          },
        
    };

 
    static navigationOptions = ()=>{
         
           return ({title:'StreetPal' });
        }


    setScenario(code /*: ?string */) {
      
        if (!code) return;
        
        const scenario = scenarios(strs[this.props.lang1])[code];


        const chatList = this.state.chatList.slice();

        let optionsCount = 0;
        let selectedOption = [];
        for (const item of chatList) {
           
            if (item.style !== 'OPTION') break;
            optionsCount += 1;
            if (item.next == code) {
                selectedOption  = [item.value];
             }
           
 
        }
        
     
        if (scenario) {
            additions=[]  
            this.setState({
                scenario: code,
                chatList: [
                    ...additions,
                    ...selectedOption.map(value => ({ style: 'SENT', value})),
                    ...chatList.slice(optionsCount),
                ],
            }) 
            const additions = [...scenario];
            const x=additions.length;
            
            if(x>0){
              
            setTimeout(()=>{
                if(additions[i].style !='OPTION'){
                    
                additions=[additions[i]]
           
                this.setState({
                    scenario: code,
                    chatList: [
                        ...additions,
                        ...selectedOption.map(value => ({ style: 'SENT', value})),
                        ...chatList.slice(optionsCount),
                    ],
                })
                i++;
              
                if(i<x){
                    this.setScenario(code /*: ?string */)
                }
            }
                
                else{
                    additions1 = [ additions[i] ];
                   
                    for(x=i+1 ; x<additions.length ; x++){ 
                        
                        additions1.unshift(additions[x]);
                    }
                    
                    
                      
                      this.setState({
                          scenario: code,
                          chatList: [
                              ...additions1,
                              ...selectedOption.map(value => ({ style: 'SENT', value})),
                              ...chatList.slice(optionsCount),
                          ],
                      })
        

                }
                
            },1200)
        }
    
        }


        
           
        
    }
    
    setScenario1(code /*: ?string */) {
        i=0
               //console.log(code)
               switch(code){
                case 'CALL_NAZRA':{
                 console.log("CALL_NAZRA") ;
                 RNImmediatePhoneCall.immediatePhoneCall('01011910917');
                break}
                
               
                
                case 'CALL_TRUSTED_CONTACT':{
                    console.log('CALL_TRUSTED_CONTACT');
                    this.call_trusted()
                break}
                    
                case 'POLICE_STATION':{
                    console.log('POLICE_STATION') ;
                    this.props.navigation.navigate('Safe2')
                break  }
                    
                case 'CALL_NADEEM':{
                    console.log('CALL_NADEEM');
                    RNImmediatePhoneCall.immediatePhoneCall('01006662404');
                break  }
                    
                case 'HOSPITAL':{
                    console.log('HOSPITAL');
                    this.props.navigation.navigate('Safe')
                break}
    
                case 'TERMINATE_CHAT':{
                    console.log('TERMINATE_CHAT');
                    this.setState({scenario: ''})
                break}
                case 'SEND_STRESS_SIGNAL':{
                    console.log('SEND_STRESS_SIGNAL');
                    this.send_signal()
                    this.setScenario(code)
                break}
    
                default:{
                    this.setScenario(code)}
            }
    


 
     
  
    }

call_trusted(){
    if(this.props.token){
        axios.get('https://streetpal.org/api/me/trusted', { 'headers': { 'Authorization':'Bearer ' + this.props.token} })
        .then(resp=>{
            RNImmediatePhoneCall.immediatePhoneCall(resp.data.phone); })  
        .catch(err=>{console.log(err.response.data)})
    }
    else{
    AsyncStorage.getItem('app_token')
    .then(token =>{
    axios.get('https://streetpal.org/api/me/trusted', { 'headers': { 'Authorization':'Bearer ' + token} })
    .then(resp=>{
        RNImmediatePhoneCall.immediatePhoneCall(resp.data.phone); })  
    .catch(err=>{console.log(err.response.data)}
        
    )  
   })
}
}

watchID : ?number =null
componentWillUnmount () {
  navigator.geolocation.clearWatch(this.watchID)
}
send_signal(){
    navigator.geolocation.getCurrentPosition((position) =>{
        var lat = parseFloat(position.coords.latitude)
        var long = parseFloat(position.coords.longitude)
  
        var initialRegion ={
          latitude : lat,
          longitude : long,
        }
        this.setState({Position: initialRegion})
      },
  
      {enableHighAccuracy: true, timeout:20000 , maximumAge:1000})
  
      this.watchID = navigator.geolocation.watchPosition((position) => {
        // Create the object to update this.state.mapRegion through the onRegionChange function
      var lat =parseFloat(position.coords.latitude)
      var long = parseFloat (position.coords.longitude)
  
      var lastRegion ={
        latitude : lat ,
        longitude : long,
      
      }
      this.setState({Position: lastRegion})
      console.log(this.state.Position)
      AsyncStorage.getItem('app_token')
      .then(token =>{
          const lat=this.state.Position.latitude;
          const lon=this.state.Position.longitude
      axios.post('https://streetpal.org/api/request-help',{severity:'high',lat:lat,lon:lon}, { 'headers': { 'Authorization':'Bearer ' + token} })
      .then(resp=>console.log(resp.data))
      .catch(err=>console.log(err))  
    })
      }
      
      );
}
    //set(code){  }

    render() {
        const s = strs[this.props.lang1];

        if (!this.state.scenario) {
            return (
                <SafeAreaView style={{ flex: 1 ,backgroundColor:'#fff'}}>
                    <View style={styles.scenarioContainer}>
                        <View style={{justifyContent:'center',margin:10,alignItems:'center',width:Dimensions.get('window').width}} >
                            <Text style={{color:'#000'}}>{s.Welcome}</Text>
                            <Text style={{color:'#aaa'}} >{s.Tell}</Text>
                        </View>
                        <BigButton value={s.scenario_feels_followed} onPress={() => this.setScenario1('USER_FEELS_FOLLOWED')} />
                        <BigButton value={s.scenario_feels_danger} onPress={() => this.setScenario1('USER_FEELS_IN_DANGER')} />
                        <BigButton value={s.scenario_been_harrassed} onPress={() => this.setScenario1('USER_HAS_BEEN_HARASSED')} />
                    </View>
                </SafeAreaView>
            );
        } 
        
        else {
            if(this.state.scenario== 'USER_HAS_BEEN_HARASSED'){
                return (
                    <SafeAreaView style={{ flex: 1 ,backgroundColor:'#fff'}}>
                        <ScrollView style={{  flex: 1,paddingHorizontal: 5}}>
                            <View style={{justifyContent:'center',margin:10,alignItems:'center',width:Dimensions.get('window').width}} >
                                <Text style={{color:'#000'}}>{s.Welcome}</Text>
                                <Text style={{color:'#aaa'}} >{s.Tell}</Text>
                            </View>
                            <SmallButton value={s.scenario_feels_followed} onPress={() => this.setScenario1('USER_IS_INJURED')} />
                            <SmallButton value={s.scenario_feels_danger} onPress={() => this.setScenario1('USER_IN_POLICE_STATION')} />
                            <SmallButton value={s.scenario_been_harrassed} onPress={() => this.setScenario1('USER_FILED_REPORT')} />
                            <SmallButton value={s.scenario_feels_followed} onPress={() => this.setScenario1('USER_IS_INJURED')} />
                            <SmallButton value={s.scenario_feels_danger} onPress={() => this.setScenario1('USER_NEEDS_TO_TALK')} />
                           
                        </ScrollView>
                    </SafeAreaView>
                );
            }
            // return (<Text>{JSON.stringify(this.state.chatList)}</Text>)
            return <SafeAreaView style={{ flex: 1 }}>
                <FlatList
                   // scrollToEnd([params])
                    inverted={true}
                    data={this.state.chatList}
                    keyExtractor={({ style }, k) => style + k}
                    renderItem={({ item: { style, value, next }, index }) =>
                        style === 'RECEIVED' ?
                            <ChatReceived value={value} next={next} index={index} cb={this.setScenario1.bind(this)} />
                            : style === 'SENT' ?
                                <ChatSent value={value} next={next} index={index} />
                                : style === 'OPTION' && next ?
                                    <ChatOption value={value} next={next} index={index} cb={this.setScenario1.bind(this)} />
                                    : null
                    }
                />

            </SafeAreaView>;
        }
    }
}

const styles = StyleSheet.create({
    scenarioContainer: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 5,
    },
    bigButtonContainer: {
        flexDirection: 'row', alignItems : 'center',
        // alignItems: 'center',
        backgroundColor: '#FDAC60',
        padding: 20,
    },
    bigButtonText: {
        color: '#fff',
       paddingLeft:20,paddingRight:20
    },
    smallButtonContainer :{
        flexDirection: 'row', alignItems : 'center',
        // alignItems: 'center',
        backgroundColor: '#eee',
        padding: 10,
    },
    smallButtonText: {
        color: '#222',
       padding:10
    },
    chatOptionConatiner: {
        backgroundColor: '#FDAC60',
        borderRadius: 15,
        borderTopLeftRadius: 5,
        // color: '#fff',
        padding: 10,
        margin: 5,
    },
    chatOptionText: {},
});

export default connect(state => ({
    lang1: state.lang.language,
    token : state.auth.token,
}) )(Chat);
