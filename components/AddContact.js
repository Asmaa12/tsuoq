import React, { Component } from 'react';
import ContactsWrapper from 'react-native-contacts-wrapper';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,FlatList,Image,Alert,AsyncStorage
} from 'react-native';
import axios from 'axios';
import { TrustItem } from './common/trustContItem';
import Icon from 'react-native-fa-icons';
import { connect } from 'react-redux';
import { Loading} from './common';

var customData = require('./string/AddContact.json');

var data = customData.English



class addContact extends Component {
        
        state ={
            tcontact :[ ],
            AuthStr:'',
            progressVisible:true,
    }
 
    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic

        }
        else{
            data=customData. English
        }

        AsyncStorage.getItem('app_token')
        .then(token =>{
            this.setState({AuthStr:'Bearer ' + token}) 
            axios.get('https://streetpal.org/api/me/trusted', { 'headers': { 'Authorization': this.state.AuthStr } })
            .then(resp=>{
                this.setState({progressVisible:false});
                const tcontact =this.state.tcontact.slice() ;
                tcontact.push({Name: resp.data.name , phoneNumber :resp.data.phone,email:resp.data.email});
                this.setState({tcontact : tcontact}) 
                    })

            .catch(err=>{}
                        //console.log(err.response.data)
                    ) 
                })
                
    
    }

    componentDidMount() {
   
                this.props.navigation.setParams({done:this.done.bind(this)}); 
                this.props.navigation.setParams({cancel:this.cancel.bind(this)}); 
                //this.props.navigation.setParams({data: this.props.lang1})

               
              }

   
  
    static navigationOptions = ({navigation})=>{
       
        const {params ={}} =navigation.state;
       
         const headerRight =(
             <TouchableOpacity onPress={params.done}>
              <Text style={{color:'#fff',fontSize:15,fontWeight:'600',padding:10}}>{data.x2}  </Text>
         </TouchableOpacity>
           
                 );
         const headerLeft =(
                 <TouchableOpacity onPress={params.cancel}>
                     <Text style={{color:'#fff',fontSize:15,fontWeight:'600',padding:10}}>{data.x3}</Text>
                </TouchableOpacity>
                 );         
                           
         
             return ({headerRight:headerRight , headerLeft:headerLeft ,title: data.x1 });
        
         }

    
         cancel(){
            this.props.navigation.navigate('Profile')
        }   
        
        done(){
            
            const tcontact =this.state.tcontact.slice() ;
            if(tcontact[0]){
                this.setState({progressVisible:true})
                axios.post('https://streetpal.org/api/me/trusted',{name:tcontact[0].Name ,email:tcontact[0].email, phone:tcontact[0].phoneNumber}, { 'headers': { 'Authorization': this.state.AuthStr } })
                .then(resp=>{
                    this.props.navigation.navigate('Profile');
                    this.setState({progressVisible:false})
                        })

                .catch(err=>{
                    this.setState({progressVisible:false})
                }
                        
                    )
                }

        } 

    onButtonPressed() {
        
       const tcontact =this.state.tcontact.slice() ;
       if(!tcontact[0]){
        ContactsWrapper.getContact()
        .then((contact) => {
        
           
            tcontact.push({Name: contact.name , phoneNumber : contact.phone});
            this.setState({tcontact : tcontact}) 
             
            console.log(contact);
        })
        .catch((error) => {
            console.log("ERROR CODE: ", error.code);
            console.log("ERROR MESSAGE: ", error.message);
        });
    }}

    
    
    renderShatItem=({item,index})=>{
      
        const tcontact = this.state.tcontact.slice();
        return (<TrustItem Name={item.Name} phoneNumber={item.phoneNumber} 
            onPress={()=>{ 
               
                Alert.alert(
                    '',
                    data.x4,
                    [
                        {text: data.x5 , onPress: ()=>console.log('cancel')},
                        {text: data.x6 , onPress: ()=>{
                            tcontact.splice(index,1);
                            this.setState({tcontact})
                        }}
                    ],
                    {cancelable: true}
                )
               }
            }
            />)
      
      }
      load=()=>{
        
        if(this.state.progressVisible){
          return (
            <Loading>{data.x8}</Loading>
         )
        }
  
      }

      key_Extractor=(item,index,)=>index  //item.id
    
    render() {
     
      
        return (
            <View style = {styles.container}>
                <TouchableOpacity onPress = {this.onButtonPressed.bind(this)}>
                <View style={styles.imagecont}>
                <Icon name='user-plus'  style={{ color:'#fff',fontSize: 40, }} />
                
                </View>
                </TouchableOpacity>
                <Text style={styles.contt}>{data.x7}</Text>
                <FlatList
                    data={this.state.tcontact}
                    renderItem={this.renderShatItem}
                    keyExtractor ={this.key_Extractor}
                    />

                {this.load()}    
            </View>

        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
   alignItems: 'center',
    
  },
  image:{
      width:70,height:70,
      opacity:.8
  },
  imagecont:{
    backgroundColor:'#FDAC60',
    borderRadius:50,
    width:100,height:100,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth:5,
    borderColor:'rgba(249, 176, 76,.3)',
    marginTop:50,
  },
  contt:{
    
    padding:15,
    fontSize:15,
    fontWeight:'600',
    justifyContent : 'center',
    alignItems : 'center',
    color:'#000',
    
},

  
});



const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        
    }
}

export default connect(mapStateToProps, { })(addContact);