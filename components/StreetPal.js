import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, navigator, BackHandler, Platform
} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import Icon from 'react-native-fa-icons';
import SafeArea from 'react-native-safe-area';

import SafePlaces from './SafePlaces';
import Home from './Home';
import Library from './Library';
import { connect } from 'react-redux';
var customData = require('./string/StreetPal.json');
var data = customData.English



class StreetPal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Home',
            marginBottom: 0,
        };

        SafeArea.getSafeAreaInsetsForRootView().then(this.updateSafeArea.bind(this));
    }

    updateSafeArea(data) {
        console.log(data)
        this.setState({ marginBottom: data.safeAreaInsets.bottom });
    }

    componentWillMount() {
        SafeArea.addEventListener('safeAreaInsetsForRootViewDidChange', this.updateSafeArea.bind(this));

        if (this.props.lang1 == 'Arabic') {
            data = customData.Arabic

        }
        else {
            data = customData.English

        }


        //console.log("language  "+locale)
    }

    componentWillUnmount() {
        SafeArea.removeEventListener('safeAreaInsetsForRootViewDidChange', this.updateSafeArea.bind(this));
    }

    static navigationOptions = ({ navigation }) => {

        const { params = {} } = navigation.state;

        const header = (
            <TouchableOpacity onPress={params.setting}>
                <Image source={require('./photo/setting2.png')} style={styles.setting} />
            </TouchableOpacity>
        );

        if (params.reverse) {
            return ({ headerLeft: header, headerRight: null, title: data.x1 });
        }
        else {
            return ({ headerRight: header, headerLeft: null, title: data.x1 });
        }
    }

    handleBack() {
        BackHandler.exitApp();
    }
    componentWillUnMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBack);

    }
    componentDidMount() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBack);   //handle android back button

        this.props.navigation.setParams({ setting: this.setting.bind(this) });
        this.props.navigation.setParams({ reverse: this.props.reverse });
    }




    setting() {
        this.props.navigation.navigate('Setting')
    }


    render() {

        if (this.props.reverse) {
            return (
                <View style={{ flex: 1 }}>
                    <TabNavigator style={styles.container}>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'Library'}
                            title={data.x4}
                            tabStyle={{ backgroundColor: '#fff' }}
                            selectedTitleStyle={{ color: '#FDAC60', fontSize: 12, }}
                            renderIcon={() => <Icon name='book' style={{ color: "#666", fontSize: 21, }} />}
                            renderSelectedIcon={() => <Icon name='book' style={{ color: '#FDAC60', fontSize: 28, }} />}
                            onPress={() => { this.setState({ selectedTab: 'Library' }) }}>
                            <Library />
                        </TabNavigator.Item>


                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'Safe Places'}
                            title={data.x3}
                            tabStyle={{ backgroundColor: '#fff' }}
                            selectedTitleStyle={{ color: "#FDAC60", fontSize: 12, }}
                            renderIcon={() => <Icon name='map-marker' style={{ color: "#666", fontSize: 22, }} />}
                            renderSelectedIcon={() => <Icon name='map-marker' style={{ color: "#FDAC60", fontSize: 27, }} />}
                            onPress={() => { this.setState({ selectedTab: 'Safe Places' }) }}>
                            <SafePlaces />
                        </TabNavigator.Item>

                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'Home'}
                            title={data.x2}
                            tabStyle={{ backgroundColor: '#fff' }}
                            selectedTitleStyle={{ color: "#FDAC60", fontSize: 12, }}
                            renderIcon={() => <Icon name="home" style={{ color: "#666", fontSize: 22, }} />}
                            renderSelectedIcon={() => <Icon name='home' style={{ color: "#FDAC60", fontSize: 29, }} />}
                            //badgeText="1"
                            onPress={() => { this.setState({ selectedTab: 'Home' }) }}>
                            <Home navigation={this.props.navigation} />
                        </TabNavigator.Item>

                    </TabNavigator>
                    <View style={{ height: this.state.marginBottom, backgroundColor: '#fff' }} />
                </View >
            );

        }

        else {
            return (
                <View style={{ flex: 1 }}>
                    <TabNavigator style={styles.container}>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'Home'}
                            title={data.x2}
                            tabStyle={{ backgroundColor: '#fff' }}
                            selectedTitleStyle={{ color: "#FDAC60", fontSize: 12, }}
                            renderIcon={() => <Icon name="home" style={{ color: "#666", fontSize: 22, }} />}
                            renderSelectedIcon={() => <Icon name='home' style={{ color: "#FDAC60", fontSize: 29, }} />}
                            //badgeText="1"
                            onPress={() => { this.setState({ selectedTab: 'Home' }) }}>
                            <Home navigation={this.props.navigation} />
                        </TabNavigator.Item>

                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'Safe Places'}
                            title={data.x3}
                            tabStyle={{ backgroundColor: '#fff' }}
                            selectedTitleStyle={{ color: "#FDAC60", fontSize: 12, }}
                            renderIcon={() => <Icon name='map-marker' style={{ color: "#666", fontSize: 22, }} />}
                            renderSelectedIcon={() => <Icon name='map-marker' style={{ color: "#FDAC60", fontSize: 27, }} />}
                            onPress={() => { this.setState({ selectedTab: 'Safe Places' }) }}>
                            <SafePlaces />
                        </TabNavigator.Item>

                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'Library'}
                            title={data.x4}
                            tabStyle={{ backgroundColor: '#fff' }}
                            selectedTitleStyle={{ color: '#FDAC60', fontSize: 12, }}
                            renderIcon={() => <Icon name='book' style={{ color: "#666", fontSize: 21, }} />}
                            renderSelectedIcon={() => <Icon name='book' style={{ color: '#FDAC60', fontSize: 28, }} />}
                            onPress={() => { this.setState({ selectedTab: 'Library' }) }}>
                            <Library />
                        </TabNavigator.Item>

                    </TabNavigator>
                    <View style={{ height: this.state.marginBottom, backgroundColor: '#fff' }} />
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',

    },
    setting: {
        width: 22,
        height: 22,
        margin: 15,
        justifyContent: 'center',
    }

});

const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        reverse: state.lang.reverse
    }
}

export default connect(mapStateToProps, {})(StreetPal);

