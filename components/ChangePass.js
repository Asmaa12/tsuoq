import React ,{Component} from 'react';
import {Text,StyleSheet,View, ColorPropType, Image,Animated, TouchableOpacity, AsyncStorage, TextInput,ScrollView} from 'react-native';
import axios from 'axios';
import { connect } from 'react-redux';
var customData = require('./string/ChangePass.json');

import {Button,Card ,CardItem,Input,Spinner, CardList} from './common';


var data = customData.English

class ChangePass extends Component{
    constructor(){
        super();
        this.state ={
            CurrentPassword: '',
            NewPassword: '',
            ConfirmPassword:'',
            loading: false,
            error:''
            
        };
    }
    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{
            data=customData. English
        
            }
  
    }
   
    componentDidMount() {
   
                this.props.navigation.setParams({done:this.done.bind(this)}); 
                this.props.navigation.setParams({cancel:this.cancel.bind(this)}); 
                this.props.navigation.setParams({ reverse: this.props.reverse });
              }
   
              
         
    
    static navigationOptions = ({navigation})=>{
       
               
                
                 const {params ={}} =navigation.state;
                
                 const headerRight =(
                     <TouchableOpacity onPress={params.done}>
                      <Text style={{color:'#fff',fontSize:15,fontWeight:'600',padding:10}}>{data.x2}  </Text>
                 </TouchableOpacity>
                   
                         );
                 const headerLeft =(
                         <TouchableOpacity onPress={params.cancel}>
                             <Text style={{color:'#fff',fontSize:15,fontWeight:'600',padding:10}}>{data.x3}</Text>
                        </TouchableOpacity>
                         );         
                                   
                if(params.reverse) {
                    return ({headerRight:headerLeft , headerLeft:headerRight ,title:data.x1 });
                }
                else{
                    return ({headerRight:headerRight , headerLeft:headerLeft ,title:data.x1 });
                }
                     
                
                 }
       
        
        done(){
            
         
                 
            let CurrentPass=this.state.CurrentPassword;
            let NewPass=this.state.NewPassword;
            let ConfirmPass =this.state.ConfirmPassword;
            this.setState({loading:true})
            this.setState({error:''})

            if(CurrentPass==''){
                return( this.setState({error:data.x4}),this.setState({loading:false}))
            }
            else if(NewPass==''){
                return( this.setState({error:data.x5}),this.setState({loading:false}))
            }
            else if(ConfirmPass==''){
                return( this.setState({error:data.x6}),this.setState({loading:false}))
            }
            else if(!( (CurrentPass.toUpperCase() != CurrentPass)&&
            (CurrentPass.toLowerCase()!=CurrentPass)&&
            (/\d/.test(CurrentPass))&&(CurrentPass.length>8) )){
                return(
                this.setState({error:data.x7}),
                this.setState({CurrentPass:''}),
                this.setState({loading:false})
                )
            }
            else if(!( (NewPass.toUpperCase() != NewPass)&&
            (NewPass.toLowerCase()!=NewPass)&&
            (/\d/.test(NewPass))&&(NewPass.length>8) )){
                return(
                this.setState({error:data.x8}),
                this.setState({NewPassword:''}),
                this.setState({loading:false})
                )
            }
            else if(NewPass != ConfirmPass){
                return(
               this.setState({error:data.x9}),
               this.setState({confirmPassword:''}),
               this.setState({loading:false})
                )
           }

          else{
          
            AsyncStorage.getItem('app_token')
            .then(token=>
                axios.post('https://streetpal.org/api/me/pass',{ 'old-pass':CurrentPass, 'new-pass':NewPass},{ 'headers': { 'Authorization': 'Bearer ' + token } })
                .then(resp=>{
                    console.log(resp), 
                    this.props.navigation.navigate('Profile')})
                .catch(err=>{console.log('error'),this.setState({loading:false,error:data.x10})})
            )
              
            }
            
          
            }
        
        cancel(){
            this.props.navigation.navigate('Profile')
        }

        _renderLoading(){
            if(this.state.loading){
                return <Spinner/>
            }
        }
    render(){
        
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
        }
        else{ AsyncStorage.getItem('language')
        .then(lang=>{
            if(lang=='Arabic'){
            data=customData. Arabic
        }
            else{
                data=customData. English
            }
    })
}
        
        return(
             <ScrollView  style={{backgroundColor:'#eee'}}>
                 
                 <View style={{flexDirection :'row',backgroundColor:'#fff',marginBottom:30,marginTop:30}}>
                 <Image source={require('./photo/pass.png')} style={styles.image}/>
                 <Input 
                 onChangeText ={(CurrentPassword)=> this.setState({CurrentPassword})}
                 placeholder ={data.x11}
                 editable ={!(this.state.loading)} 
                 value={this.state.CurrentPassword}
                 securetext = {true}/>
                 </View>

            <View style={{flexDirection :'row',backgroundColor:'#fff',marginBottom:2}}>
            <Image source={require('./photo/pass.png')} style={styles.image}/>
            <Input 
            onChangeText ={(NewPassword=> this.setState({NewPassword}))}
            placeholder ={data.x12}
            value={this.state.NewPassword}
            editable ={!(this.state.loading)} 
            securetext = {true}/>
            </View>

            <View style={{flexDirection :'row',backgroundColor:'#fff'}}>
            <Image source={require('./photo/pass.png')} style={styles.image}/>
            <Input 
            onChangeText ={(ConfirmPassword)=> this.setState({ConfirmPassword})}
            placeholder ={data.x13}
            editable ={!(this.state.loading)} 
            value={this.state.ConfirmPassword}
            securetext = {true}/>
            </View>
            <Text style={{color:'red',textAlign: 'center',}}>{this.state.error}</Text>
            {this._renderLoading()}
            </ScrollView>
        )
    }}
    const styles = StyleSheet.create({
        
         contain :{
             flex :1,
             justifyContent : 'center',
             alignItems : 'center',
             backgroundColor : '#fff'
             
         
         },
         
         error :{
             color : 'red',
             
          },
     
         image:{
             marginTop:10,
             marginLeft:10,
             width:28,
             height:28,
             opacity : .4
         },
     
         logo:{
             width:100,
             height:130,
             marginTop:40,  
         },
     
         street:{
             
             fontSize : 25 , fontWeight :'100',color:'#555'
             
         },
         text1:{
             fontSize : 16 , 
             margin:20,
         },
         text2:{
             color:'#000',margin:10,
             fontSize : 15 , fontWeight :'bold',
         },
         bu:{marginTop:20,
             justifyContent :'flex-start',
             flexDirection :'row',
             flex :1,
         },
         st:{
             marginLeft : 11, marginRight : 11
         }
     });
     const mapStateToProps = state => {
        return {
            lang1: state.lang.language,
            reverse:state.lang.reverse
        }
    }
    export default connect(mapStateToProps, {})(ChangePass);
    