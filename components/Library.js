import React ,{Component} from 'react';
import {Text,StyleSheet,View, ColorPropType, Image,Animated, TouchableOpacity, AsyncStorage} from 'react-native';
import TabLib from './TabLib';
import { connect } from 'react-redux';
import { Change_lang} from './actions';

var customData = require('./string/Library.json');
var data = customData.English

class Library extends Component{
  
    
    componentWillMount(){
        if(this.props.lang1=='Arabic'){
            data=customData.Arabic
          
        }
    
            else{
                data=customData. English
                
            }
  
    }

    render(){
       
       
    return(
        
        <View style={styles.tabBarContainer}>
         <View >
             <Text style={styles.text}>{data.x}</Text>
         </View>
            
            <TabLib/>
            
         
        </View>  
        )}
}

const styles=StyleSheet.create({
text:{
    color:'#999',
    fontSize:15,
    padding: 15,
    marginLeft: 50,
    marginRight:50,
    textAlign: 'center',
    justifyContent : 'center',
    alignItems : 'center',
},


icon:{
    width:27, height:27
},
tabBarContainer: {
    flex:1,
    justifyContent: 'center',
   
   
  }
})

const mapStateToProps = state => {
    return {
        lang1: state.lang.language,
        
    }
}

export default connect(mapStateToProps, { Change_lang})(Library);
